# -*- mode: python ; coding: utf-8 -*-


block_cipher = None


a = Analysis(
    ['D:/Proyecto Final/PyRehab/src/pyrehab.py'],
    pathex=['D:/Proyecto Final/PyRehab/src', 'D:/Proyecto Final/PyRehab/sql', 'D:/Proyecto Final/PyRehab/src/Plugins', 'D:/Proyecto Final/PyRehab/venv/Lib/site-packages'],
    binaries=[],
    datas=[('D:/Proyecto Final/PyRehab/src/assets', 'assets/'), ('D:/Proyecto Final/PyRehab/src/Plugins', 'Plugins/')],
    hiddenimports=['yaml', 'PluginABC', 'PluginABC.PluginABC', 'glob2', 'math', 'random'],
    hookspath=[],
    hooksconfig={},
    runtime_hooks=[],
    excludes=[],
    win_no_prefer_redirects=False,
    win_private_assemblies=False,
    cipher=block_cipher,
    noarchive=False,
)
pyz = PYZ(a.pure, a.zipped_data, cipher=block_cipher)

exe = EXE(
    pyz,
    a.scripts,
    [],
    exclude_binaries=True,
    name='pyrehab',
    debug=False,
    bootloader_ignore_signals=False,
    strip=False,
    upx=True,
    console=False,
    disable_windowed_traceback=False,
    argv_emulation=False,
    target_arch=None,
    codesign_identity=None,
    entitlements_file=None,
    icon=['D:\\Proyecto Final\\logo\\logoverde.ico'],
)
coll = COLLECT(
    exe,
    a.binaries,
    a.zipfiles,
    a.datas,
    strip=False,
    upx=True,
    upx_exclude=[],
    name='pyrehab',
)
