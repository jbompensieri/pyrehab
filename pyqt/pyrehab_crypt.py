"""pyrehab_crypt.py 
Funciones para encriptar y desencriptar datos.
Utiliza Fernet, un método de encriptación simétrica a partir de una llave o 'key' 
secreta que es única para poder desencriptar los datos.
"""

from cryptography.fernet import Fernet
import os


class pyrehab_crypt:
    """Clase pyrehab_crypt usada para encriptar y desencriptar datos."""

    def __init__(self):
        """
        Inicialización
        """
        self.f = Fernet(self.get_key())

    def get_key(self):
        """
        Obtiene la 'key' de un archivo. Si no existe, la y almacena
        : return: 'Key' para encriptar y desencriptar datos
        """
        fname = "crypt.key"  #: Nombre de la 'key'
        data_dir = os.path.dirname(__file__)  #: Dirección a la carpeta pyqt
        fpath = os.path.join(data_dir, fname)  #: Dirección donde se almacena la 'key'
        if os.path.isfile(fpath):
            with open(fpath, "r") as f:
                return f.read()
        else:
            key = Fernet.generate_key()  #: Genera la 'key'
            with open(fpath, "w") as f:
                f.write(key.decode())
            return key

    def encrypt(self, msg):
        """
        Devuelve el dato encriptado.
        : param msg: Mensaje o dato a encriptar
        : return: Mensaje o dato encriptado
        """
        return self.f.encrypt(msg.encode())

    def decrypt(self, msg):
        """
        Devuelve el dato desencriptado.
        : param msg: Mensaje o dato a desencriptar
        : return: Mensaje o dato desencriptado
        """
        return self.f.decrypt(msg).decode()


#: Correr por primera vez para obtener la contraseña del administrador.
# if __name__ == "__main__":
#     pc_o = pyrehab_crypt()
#     print(str(pc_o.encrypt("admin"))) #: Copiar el resultado en la base de datos
