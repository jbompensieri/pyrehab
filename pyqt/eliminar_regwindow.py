"""eliminar_regwindow.py 
Ventana en PyQT para eliminar un profesional"""

from PyQt5.QtWidgets import *
from PyQt5 import QtGui
from pyqtutils import *
from doc_user import doc_user


class EliminarRegWindow(QWidget):
    """Clase Eliminar Reg Window para ventana que permite eliminar un registro de un profesional."""

    def __init__(self):
        """
        Inicialización
        """
        super().__init__()
        self.doc_user_o = doc_user()
        self.createSearchGroupBox()
        self.createDataGroupBox()
        self.createButtonGroupBox()

        topLayout = QHBoxLayout()
        topLayout.addStretch(1)

        mainLayout = QGridLayout()
        mainLayout.addLayout(topLayout, 0, 0, 1, 2)
        mainLayout.addWidget(self.searchGroupBox, 1, 0)
        mainLayout.addWidget(self.dataGroupBox, 1, 1)
        mainLayout.addWidget(self.buttonGroupBox, 2, 0, 2, 0)
        mainLayout.setRowStretch(1, 1)
        mainLayout.setRowStretch(2, 1)
        mainLayout.setColumnStretch(0, 1)
        mainLayout.setColumnStretch(1, 1)
        self.setLayout(mainLayout)
        self.setWindowIcon(QtGui.QIcon(logo))
        self.setWindowTitle("Eliminar Registro")

        styleComboBox = QComboBox()
        styleComboBox.addItems(QStyleFactory.keys())
        styleComboBox.activated[str].connect(changeStyle)
        changeStyle()

        self.dataGroupBox.setDisabled(True)

    def createSearchGroupBox(self):
        """
        Cuadro para grupo de búsqueda de usuario por DNI
        """
        self.searchGroupBox = QGroupBox("Búsqueda de registro")

        self.lineEditUser = QLineEdit()
        self.lineEditUser.setPlaceholderText("DNI")

        self.buscarPushButton = QPushButton("Buscar")
        self.buscarPushButton.setDefault(True)
        #: Conexión del botón de búsqueda
        self.buscarPushButton.clicked.connect(self.search_user)

        layout = QGridLayout()
        layout.addWidget(self.lineEditUser, 0, 0, 1, 1)
        layout.addWidget(self.buscarPushButton, 0, 1, 1, 2)
        layout.setRowStretch(5, 1)
        self.searchGroupBox.setLayout(layout)

    def createDataGroupBox(self):
        """
        Cuadro para mostrar los datos del usuario buscado
        """
        self.dataGroupBox = QGroupBox("Datos del usuario")

        self.lineEditNombre = QLineEdit()
        self.lineEditApellido = QLineEdit()
        self.checkBoxAdmin = QCheckBox("Admin")

        layout = QGridLayout()
        layout.addWidget(self.lineEditNombre, 1, 0, 1, 2)
        layout.addWidget(self.lineEditApellido, 2, 0, 1, 2)
        layout.addWidget(self.checkBoxAdmin, 3, 0, 1, 2)

        self.dataGroupBox.setLayout(layout)

    def createButtonGroupBox(self):
        """
        Cuadro para mostrar los botones
        """
        self.buttonGroupBox = QGroupBox()
        self.eliminarPushButton = QPushButton("Eliminar")
        self.eliminarPushButton.setDefault(False)
        self.cancelarPushButton = QPushButton("Cancelar")
        self.cancelarPushButton.setDefault(False)

        layout = QGridLayout()
        layout.addWidget(self.eliminarPushButton, 1, 0, 1, 2)
        layout.addWidget(self.cancelarPushButton, 2, 0, 1, 2)

        self.buttonGroupBox.setLayout(layout)
        self.buttonGroupBox.setDisabled(True)

        self.eliminarPushButton.clicked.connect(
            self.delete_user
        )  #: Conexión del botón de eliminar usuario

        self.cancelarPushButton.clicked.connect(
            self.close
        )  #: Conexión del botón cancelar

    def search_user(self):
        """
        Función para buscar profesional por DNI
        """
        try:
            user = int(self.lineEditUser.text())
        except ValueError:
            error("DNI ingresado erróneamente")
            return
        self.buttonGroupBox.setDisabled(False)  #: Habilita los botones
        try:
            nombre, apellido, is_admin = self.doc_user_o.buscar_profesional(
                user=user
            )  #: Busca el usuario por DNI
        except ValueError:
            error(msg="Registro inexistente")
            self.lineEditNombre.setPlaceholderText("Nombre")
            self.lineEditApellido.setPlaceholderText("Apellido")
            return
        if is_admin:
            self.checkBoxAdmin.setChecked(True)
        self.lineEditNombre.setPlaceholderText(nombre)
        self.lineEditApellido.setPlaceholderText(apellido)

    def delete_user(self):
        """
        Función para eliminar profesional
        """
        user = int(self.lineEditUser.text())  #: DNI del usuario
        if not self.doc_user_o.eliminar_profesional(user=user):
            error(msg="Registro inexistente")
        else:
            info(msg="Usuario eliminado con éxito")
            self.close()
