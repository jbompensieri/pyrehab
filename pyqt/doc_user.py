"""doc_user.py 
Funciones para las acciones que puede realizar el usuario profesional.
Este usuario es el encargado de agregar, editar y/o eliminar los registros de los jugadores 
que pueden acceder a la aplicacion."""

import os
import sys
from pyqtutils import get_game_names
from pyrehab_crypt import pyrehab_crypt

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "sql")))
from pyrehab_sql import pyrehab_sql


class doc_user:
    """Clase doc_user que va a contener las funciones de las acciones permitidas para el usuario profesional."""

    def __init__(self):
        """
        Inicialización
        """
        self.table_name_user = (
            "user_table"  #: Nombre de la tabla en SQL con datos de los usuarios
        )
        self.table_name_prof = (
            "prof_table"  #: Nombre de la tabla en SQL con datos de los profesionales
        )
        self.sql = pyrehab_sql()
        self.plug_l = get_game_names()  #: Obtiene los nombres de los plugins (juegos)
        self.c = pyrehab_crypt()

    def login(self, user, passwd):
        """
        Ingreso del usuario profesional.
        : param user: DNI del profesional ingresado en GUI
        : param passwd: Contraseña ingresada en GUI
        : return: True si la contraseña ingresada es igual a la almacenada.
        """
        cond = f"dni = '{user}'"
        [(pwd,)] = self.sql.select_from_table(
            "prof_table", columns_l="password", condition=cond
        )  #: Obtengo contraseña almacenada en la tabla en SQL
        if passwd != self.c.decrypt(
            pwd
        ):  #: Compara contraseña ingresada con la almacenada
            return False
        return True

    def login_admin(self, user):
        """
        Ingreso del usuario profesional.
        : param user: DNI del profesional ingresado en GUI
        : return: True si el usuario es administrador.
        """
        cond = f"dni = '{user}'"
        [(is_admin,)] = self.sql.select_from_table(
            self.table_name_prof, columns_l="is_admin", condition=cond
        )  #: Obtengo valor de la columna is_admin para saber si es administrador
        if is_admin:
            return True
        return False

    def agregar_usuario(self, user_data):
        """
        Permite al profesional agregar un usuario/jugador.
        : param user_data: Información del jugador ingresada en GUI (DNI, nombre, apellido, fecha de nacimiento, niveles de los juegos)
        : return: True si el jugador fue agregado correctamente a la tabla
        """
        common_columns = user_data[0:-1]  #: Columnas de información común
        juegos_d = user_data[-1]  #: Diccionario de juego y su correspondiente nivel
        #: Agrega las columnas de información común a una nueva fila en la tabla en SQL
        if not self.sql.add_row(
            self.table_name_user, common_columns, "(dni, nombre, apellido, fecha_nac)"
        ):
            return False
        #: Para cada juego, agrega el valor del nivel en la columna correspondiente, en la fila del dni del jugador
        for juego, valor in juegos_d.items():
            self.sql.update_table(
                self.table_name_user, juego, valor, f"(dni = '{user_data[0]}')"
            )
        return True

    def registrar_profesional(self, user_data):
        """
        Permite al profesional registrar otro profesional.
        : param user_data: Información del profesional ingresada en GUI (DNI, nombre, apellido, contraseña)
        : return: True si el profesional fue agregado correctamente a la tabla
        """

        (dni, nom, ape, pwd, admin) = user_data
        pwd_crypted = self.c.encrypt(pwd)  #: Encripta la contraseña
        return self.sql.add_row(
            self.table_name_prof, (dni, nom, ape, pwd_crypted.decode("utf-8"), admin)
        )  #: Agrega la fila a la tabla con la información del profesional

    def eliminar_usuario(self, user):
        """
        Permite al profesional eliminar a un jugador.
        : param user: DNI del jugador a eliminar
        : return: True si el jugador fue eliminado correctamente a la tabla
        """
        self.sql.delete_row(
            self.table_name_user, f"(dni = '{user}')"
        )  #: Elimina la fila que corresponde al dni ingresado
        return True

    def eliminar_profesional(self, user):
        """
        Permite al administrador eliminar a un profsional.
        : param user: DNI del profesional a eliminar
        : return: True si el profesional fue eliminado correctamente a la tabla
        """
        self.sql.delete_row(
            self.table_name_prof, f"(dni = '{user}')"
        )  #: Elimina la fila que corresponde al dni ingresado
        return True

    def buscar_usuario(self, user):
        """
        Función para buscar a un jugador en la tabla por dni.
        : param user: DNI del jugador a buscar
        : return: Nombre, apellido y fecha de nacimiento del jugador
        """
        [(nombre, apellido, fecha_nac)] = self.sql.select_from_table(
            self.table_name_user, "nombre, apellido, fecha_nac", f"(dni = '{user}')"
        )  #: Selecciona la fila correspondiente al dni y extrae los valores de las columnas solicitadas
        return nombre, apellido, fecha_nac

    def buscar_resultados(self, user, game):
        """
        Función para buscar los resultados de un jugador en la tabla por dni.
        : param user: DNI del jugador a buscar
        : param game: nombre del juego, que es el nombre de la tabla
        : return: DataFrame con los datos de la tabla según el juego
        """
        res_df = self.sql.get_df(user=user, table_name=game)
        return res_df

    def buscar_profesional(self, user):
        """
        Función para buscar a un profesional en la tabla por dni.
        : param user: DNI del profesional a buscar
        : return: Nombre, apellido y contraseña del profesional
        """
        [(nombre, apellido, admin)] = self.sql.select_from_table(
            self.table_name_prof, "nombre, apellido, is_admin", f"(dni = '{user}')"
        )  #: Selecciona la fila correspondiente al dni y extrae los valores de las columnas solicitadas
        return nombre, apellido, admin

    def pedir_nivel(self, user, juego):
        """
        Función para obtener información de los niveles de los juegos de un jugador.
        : param user: DNI del jugador a buscar
        : param juego: Nombre del juego del cual se quiere obtener la información
        : return: Nivel
        """
        [(nivel)] = self.sql.select_from_table(
            self.table_name_user, juego, f"(dni = '{user}')"
        )  #: Selecciona la fila correspondiente al dni y extrae el valor de la columna del juego solicitado
        return nivel

    def editar_usuario(self, user, nombre, apellido, fecha_nac, juego_d):
        """
        Permite al profesional editar la información de un jugador.
        : param user: DNI del jugador a editar
        : param nombre: Nombre del jugador a editar
        : param apellido: Apellido del jugador a editar
        : param fecha_nac: Fecha de nacimiento del jugador a editar
        : param juego_d: Diccionario con información de los juegos, correspondiente al jugador a editar
        : return: True si el jugador fue editado correctamente en la tabla
        """
        self.sql.update_table(
            self.table_name_user, "nombre", nombre, f"(dni = '{user}')"
        )  #: Edita el nombre del jugador
        self.sql.update_table(
            self.table_name_user, "apellido", apellido, f"(dni = '{user}')"
        )  #: Edita el apellido del jugador
        self.sql.update_table(
            self.table_name_user, "fecha_nac", fecha_nac, f"(dni = '{user}')"
        )  #: Edita la fecha de nacimiento del jugador
        for juego, nivel in juego_d.items():
            self.sql.update_table(
                self.table_name_user, juego, nivel, f"(dni = '{user}')"
            )  #: Edita el nivel de cada juego
        return True

    def editar_profesional(self, user, nombre, apellido, pwd, is_admin):
        """
        Permite al administrador editar la información de un profesional.
        : param user: DNI del profesional a editar
        : param nombre: Nombre del profesional a editar
        : param apellido: Apellido del profesional a editar
        : param contrasena: Contraseña del profesional a editar
        : return: True si el jugador fue editado correctamente en la tabla
        """
        pwd_crypted = self.c.encrypt(pwd)  #: Encripta la contraseña
        self.sql.update_table(
            self.table_name_prof, "nombre", nombre, f"(dni = '{user}')"
        )  #: Edita el nombre del profesional
        self.sql.update_table(
            self.table_name_prof, "apellido", apellido, f"(dni = '{user}')"
        )  #: Edita el apellido del profesional
        if pwd:
            self.sql.update_table(
                self.table_name_prof,
                "password",
                pwd_crypted.decode("utf-8"),
                f"(dni = '{user}')",
            )  #: Edita la contraseña del profesional
        self.sql.update_table(
            self.table_name_prof, "is_admin", is_admin, f"(dni = '{user}')"
        )  #: Edita permiso de administrador
        return True
