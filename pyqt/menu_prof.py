"""menu_prof.py 
Ejecuta PyQT"""

from agregarwindow import AgregarWindow
from editarwindow import EditarWindow
from eliminarwindow import EliminarWindow
from resultadoswindow import ResultadosWindow
from registrowindow import RegistroWindow
from editar_regwindow import EditarRegWindow
from eliminar_regwindow import EliminarRegWindow
from PyQt5 import QtGui
from PyQt5.QtWidgets import *
import sys
from pyqtutils import *
from doc_user import doc_user
import webbrowser


class MenuProf(QDialog):
    """Clase MenuProf encargada de ejecutar la interfaz de usuario de los profesionales"""

    def __init__(self, parent=None):
        """
        Inicialización
        """
        super(MenuProf, self).__init__(parent)

        styleComboBox = QComboBox()
        styleComboBox.addItems(QStyleFactory.keys())

        self.createIniciarSesionBox()
        self.createOpcionesGroupBox()
        self.createAdminGroupBox()

        styleComboBox.activated[str].connect(changeStyle)

        topLayout = QHBoxLayout()
        topLayout.addStretch(1)

        mainLayout = QGridLayout()
        mainLayout.addLayout(topLayout, 0, 0, 1, 2)
        mainLayout.addWidget(
            self.iniciarSesionBox, 1, 0
        )  #: Espacio para inicio de sesión (fila=1 columna=0)
        mainLayout.addWidget(
            self.opcionesGroupBox, 1, 1
        )  #: Espacio para opciones (fila=1 columna=1)
        mainLayout.addWidget(
            self.adminGroupBox, 1, 2
        )  #: Espacio para opciones de administrador (fila=1 columna=1)
        mainLayout.setRowStretch(1, 1)
        mainLayout.setRowStretch(2, 1)
        mainLayout.setColumnStretch(0, 1)
        mainLayout.setColumnStretch(1, 1)
        self.setLayout(mainLayout)
        self.setWindowTitle("PyRehab")
        self.setWindowIcon(QtGui.QIcon(logo))
        changeStyle()

        self.opcionesGroupBox.setDisabled(True)
        self.adminGroupBox.setDisabled(True)

    def check_credentials(self):
        """
        Función para comprobar la contraseña ingresada
        """
        #: DNI del profesional ingresado en el cuadro de texto
        user = self.lineEditUser.text()
        passwd = self.lineEdit.text()  #: Contraseña ingresada en el cuadro de texto
        doc_user_o = doc_user()
        #: Comprueba que la contraseña sea correcta y habilita las opciones
        if doc_user_o.login(user=user, passwd=passwd):
            if doc_user_o.login_admin(user=user):
                self.adminGroupBox.setDisabled(False)
            self.opcionesGroupBox.setDisabled(False)
            self.ingresarPushButton.setDisabled(True)
        else:
            error(msg=f"Contraseña Incorrecta", info="Intente nuevamente")

    def createIniciarSesionBox(self):
        """
        Crea cuadro para el inicio de sesión
        """
        self.iniciarSesionBox = QGroupBox("Iniciar Sesión")
        self.lineEditUser = QLineEdit()  #: Línea de texto para el ingreso del DNI
        self.lineEditUser.setPlaceholderText("DNI")
        self.lineEdit = QLineEdit()  #: Línea de texto para el ingreso de la contraseña
        self.lineEdit.setPlaceholderText("Contraseña")
        self.lineEdit.setEchoMode(QLineEdit.Password)  #: Oculta la contraseña

        self.ingresarPushButton = QPushButton("Ingresar")
        self.ingresarPushButton.setDefault(False)
        #: Conexión del botón de ingreso con la función de comprobar contraseña
        self.ingresarPushButton.clicked.connect(self.check_credentials)
        self.salirPushButton = QPushButton("Salir")
        self.salirPushButton.setDefault(False)
        self.salirPushButton.clicked.connect(self.close)
        self.docPushButton = QPushButton("Ayuda")
        self.docPushButton.setDefault(False)
        self.docPushButton.clicked.connect(self.doc)

        layout = QGridLayout()
        layout.addWidget(self.lineEditUser, 0, 0, 1, 2)
        layout.addWidget(self.lineEdit, 1, 0, 1, 2)
        layout.addWidget(self.ingresarPushButton, 2, 0, 1, 2)
        layout.addWidget(self.salirPushButton, 3, 0, 1, 2)
        layout.addWidget(self.docPushButton, 4, 0, 1, 2)
        layout.setRowStretch(5, 1)
        self.iniciarSesionBox.setLayout(layout)

    def doc(self):
        """
        Función para abrir el link a la documentación
        """
        webbrowser.open("https://gitlab.com/jbompensieri/pyrehab/-/wikis/home")

    def show_agregar_window(self):
        """
        Función para abrir la ventana de agregar usuario
        """
        self.agregar_w = AgregarWindow()
        self.agregar_w.show()

    def show_editar_window(self):
        """
        Función para abrir la ventana de editar usuario
        """
        self.editar_w = EditarWindow()
        self.editar_w.show()

    def show_editarReg_window(self):
        """
        Función para abrir la ventana de editar profesional
        """
        self.editarreg_w = EditarRegWindow()
        self.editarreg_w.show()

    def show_eliminar_window(self):
        """
        Función para abrir la ventana de eliminar usuario
        """
        self.eliminar_w = EliminarWindow()
        self.eliminar_w.show()

    def show_eliminarReg_window(self):
        """
        Función para abrir la ventana de eliminar profesional
        """
        self.eliminar_w = EliminarRegWindow()
        self.eliminar_w.show()

    def show_resultados_window(self):
        """
        Función para abrir la ventana de resultados del usuario
        """
        self.w = ResultadosWindow()
        self.w.show()

    def show_registro_window(self):
        """
        Función para abrir la ventana de registro de profesional
        """
        self.w = RegistroWindow()
        self.w.show()

    def createOpcionesGroupBox(self):
        """
        Crea el grupo de opciones
        """
        self.opcionesGroupBox = QGroupBox("Opciones")

        agregarPushButton = QPushButton("Agregar Usuario")  #: Botón de agregar usuario
        agregarPushButton.setDefault(False)
        editarPushButton = QPushButton("Editar Usuario")  #: Botón de editar usuario
        editarPushButton.setDefault(False)
        eliminarPushButton = QPushButton(
            "Eliminar Usuario"
        )  #: Botón de eliminar usuario
        eliminarPushButton.setDefault(False)
        resultadosPushButton = QPushButton(
            "Resultados"
        )  #: Botón de resultados del usuario
        resultadosPushButton.setDefault(False)

        layout = QVBoxLayout()
        layout.addWidget(agregarPushButton)
        layout.addWidget(editarPushButton)
        layout.addWidget(eliminarPushButton)
        layout.addWidget(resultadosPushButton)
        layout.addStretch(1)
        self.opcionesGroupBox.setLayout(layout)

        agregarPushButton.clicked.connect(
            self.show_agregar_window
        )  #: Conexión del botón de agregar usuario
        editarPushButton.clicked.connect(
            self.show_editar_window
        )  #: Conexión del botón de editar usuario
        eliminarPushButton.clicked.connect(
            self.show_eliminar_window
        )  #: Conexión del botón de eliminar usuario
        resultadosPushButton.clicked.connect(
            self.show_resultados_window
        )  #: Conexión del botón de resultados del usuario

    def createAdminGroupBox(self):
        """
        Crea el grupo de opciones de administrador
        """
        self.adminGroupBox = QGroupBox("Opciones Admin")
        #: Botón de agregar profesional
        agregarPushButton = QPushButton("Agregar Registro")  #: Botón de agregar usuario
        agregarPushButton.setDefault(False)
        #: Conexión del botón de registro
        agregarPushButton.clicked.connect(self.show_registro_window)
        #: Botón de editar usuario
        editarPushButton = QPushButton("Editar Registro")
        editarPushButton.setDefault(False)
        #: Botón de eliminar usuario
        eliminarPushButton = QPushButton("Eliminar Registro")
        eliminarPushButton.setDefault(False)

        layout = QVBoxLayout()
        layout.addWidget(agregarPushButton)
        layout.addWidget(editarPushButton)
        layout.addWidget(eliminarPushButton)

        layout.addStretch(1)
        self.adminGroupBox.setLayout(layout)
        #: Conexión del botón de editar usuario
        editarPushButton.clicked.connect(self.show_editarReg_window)
        #: Conexión del botón de eliminar usuario
        eliminarPushButton.clicked.connect(self.show_eliminarReg_window)


if __name__ == "__main__":

    import sys

    app = QApplication(sys.argv)
    gallery = MenuProf()
    gallery.show()
    sys.exit(app.exec_())
