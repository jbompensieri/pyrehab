import os
import sys
from PyQt5.QtWidgets import *
from PyQt5 import QtGui

logo = os.path.abspath(os.path.join(os.path.abspath(__file__), "..", "logobig.png"))
if not os.path.isfile(logo):
    sys.exit(f"Logo not found in: {logo}")


def changeStyle(styleName="windowsvista"):
    """
    Estilo del PyQT
    """
    QApplication.setStyle(QStyleFactory.create(styleName))
    QApplication.setPalette(QApplication.style().standardPalette())


def error(msg, info=""):
    """
    Muestra mensaje de error en pantalla
    : param msg: Mensaje
    : param info: Texto informativo sobre el mensaje. Default vacío.
    """
    error_w = QMessageBox()  #: Cuadro para mensajes de error
    error_w.setIcon(QMessageBox.Critical)
    error_w.setWindowIcon(QtGui.QIcon(logo))
    error_w.setText(msg)
    error_w.setInformativeText(info)
    error_w.setWindowTitle("Error")
    error_w.exec_()


def info(msg, info=""):
    """
    Muestra mensaje informativo en pantalla
    : param msg: Mensaje
    : param info: Texto informativo sobre el mensaje. Default vacío.
    """
    info_w = QMessageBox()  #: Cuadro para mensajes informativos
    info_w.setWindowIcon(QtGui.QIcon(logo))
    info_w.setText(msg)
    info_w.setInformativeText(info)
    info_w.setWindowTitle("Mensaje")
    info_w.exec_()


def get_game_names():
    """
    Devuelve los nombres de los juegos disponibles
    : return: Lista con los nombres de los juegos
    """
    return ["diferencias", "escalas", "memotest"]
