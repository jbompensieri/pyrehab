"""editarwindow.py 
Ventana en PyQT para editar un usuario"""

from PyQt5.QtWidgets import *
from PyQt5 import QtGui
from pyqtutils import *
from PyQt5.QtCore import QDate
from doc_user import doc_user


class EditarWindow(QWidget):
    """Clase EditarWindow para ventana que permite editar los datos del usuario"""

    def __init__(self):
        """
        Inicialización
        """
        super().__init__()
        self.doc_user_o = doc_user()
        self.createSearchGroupBox()
        self.createDataGroupBox()
        self.createGamesGroupBox()
        self.createButtonGroupBox()

        topLayout = QHBoxLayout()
        topLayout.addStretch(1)

        mainLayout = QGridLayout()
        mainLayout.addLayout(topLayout, 0, 0, 1, 2)
        mainLayout.addWidget(self.searchGroupBox, 1, 0)
        mainLayout.addWidget(self.dataGroupBox, 1, 1)
        mainLayout.addWidget(self.gamesGroupBox, 1, 2)
        mainLayout.addWidget(self.buttonGroupBox, 2, 0, 2, 0)
        mainLayout.setRowStretch(1, 1)
        mainLayout.setRowStretch(2, 1)
        mainLayout.setColumnStretch(0, 1)
        mainLayout.setColumnStretch(1, 1)
        self.setLayout(mainLayout)
        self.setWindowIcon(QtGui.QIcon(logo))
        self.setWindowTitle("Editar Usuario")

        styleComboBox = QComboBox()
        styleComboBox.addItems(QStyleFactory.keys())
        styleComboBox.activated[str].connect(changeStyle)
        changeStyle()

        self.gamesGroupBox.setDisabled(True)  #: Campo inicia desactivado

    def createSearchGroupBox(self):
        """
        Cuadro para grupo de búsqueda de usuario por DNI
        """
        self.searchGroupBox = QGroupBox("Búsqueda de usuario")

        self.lineEditUser = QLineEdit()  #: Línea para escribir DNI del usuario
        self.lineEditUser.setPlaceholderText("DNI")

        self.buscarPushButton = QPushButton("Buscar")  #: Botón para buscar usuario
        self.buscarPushButton.setDefault(False)
        self.buscarPushButton.clicked.connect(
            self.search_user
        )  #: Conexión del botón con la función para buscar usuario

        layout = QGridLayout()
        layout.addWidget(self.lineEditUser, 0, 0, 1, 1)
        layout.addWidget(self.buscarPushButton, 0, 1, 1, 2)
        layout.setRowStretch(5, 1)
        self.searchGroupBox.setLayout(layout)

    def createDataGroupBox(self):
        """
        Cuadro para mostrar los datos del usuario buscado
        """
        self.dataGroupBox = QGroupBox("Datos de usuario")

        self.lineEditNombre = QLineEdit()  #: Línea para editar nombre
        self.lineEditApellido = QLineEdit()  #: Línea para editar apellido
        self.dateNacimiento = QDateEdit(
            self.dataGroupBox
        )  #: Línea para editar fecha de nacimiento
        self.dateNacimiento.setDisplayFormat("dd/MM/yyyy")
        label_1 = QLabel("Nombre")
        label_2 = QLabel("Apellido")
        label_3 = QLabel("Fecha de nac.")

        layout = QGridLayout()
        layout.addWidget(label_1, 1, 0, 1, 1)
        layout.addWidget(label_2, 2, 0, 1, 1)
        layout.addWidget(label_3, 3, 0, 1, 1)
        layout.addWidget(self.lineEditNombre, 1, 1, 1, 2)
        layout.addWidget(self.lineEditApellido, 2, 1, 1, 2)
        layout.addWidget(self.dateNacimiento, 3, 1, 1, 2)
        layout.setRowStretch(5, 1)
        self.dataGroupBox.setLayout(layout)
        self.dataGroupBox.setDisabled(True)

    def createGamesGroupBox(self):
        """
        Cuadro para introducir el nivel de cada juego
        """
        self.gamesGroupBox = QGroupBox("Juegos")
        self.QButtonGroup_d = {}
        plugin_l = get_game_names()
        layout = QGridLayout()
        i = 0
        for juego in plugin_l:
            self.QButtonGroup_d[juego] = QButtonGroup(self)
            rb1 = QRadioButton("1", self)
            rb2 = QRadioButton("2", self)
            rb3 = QRadioButton("3", self)
            self.QButtonGroup_d[juego].addButton(rb1)
            self.QButtonGroup_d[juego].setId(rb1, 1)
            self.QButtonGroup_d[juego].addButton(rb2)
            self.QButtonGroup_d[juego].setId(rb2, 2)
            self.QButtonGroup_d[juego].addButton(rb3)
            self.QButtonGroup_d[juego].setId(rb3, 3)
            self.QButtonGroup_d[juego].button(1).setChecked(True)

            label = QLabel(f"{juego}")
            layout.addWidget(label, i, 0, 1, 2)
            i += 1

            layout.addWidget(rb1, i, 0, 1, 2)
            layout.addWidget(rb2, i, 1, 1, 2)
            layout.addWidget(rb3, i, 2, 1, 2)
            i += 1

        layout.setRowStretch(i - 1, 1)
        self.gamesGroupBox.setLayout(layout)

    def createButtonGroupBox(self):
        """
        Cuadro con los botones
        """
        self.buttonGroupBox = QGroupBox()
        self.editarPushButton = QPushButton("Editar")
        self.editarPushButton.setDefault(False)
        self.cancelarPushButton = QPushButton("Cancelar")
        self.cancelarPushButton.setDefault(False)

        layout = QGridLayout()
        layout.addWidget(self.editarPushButton, 1, 0, 1, 2)
        layout.addWidget(self.cancelarPushButton, 2, 0, 1, 2)

        self.buttonGroupBox.setLayout(layout)
        #: Habilita los botones de Editar y Cancelar una vez ingresado correctamente el DNI del usuario a editar
        self.buttonGroupBox.setDisabled(True)

        self.editarPushButton.clicked.connect(
            self.edit_user
        )  #: Conexión del botón de edición
        self.cancelarPushButton.clicked.connect(
            self.close
        )  #: Conexión del botón de cancelar

    def search_user(self):
        """
        Función para buscar un usuario por DNI
        """
        try:
            #: self.user es el DNI ingresado en la línea de búsqueda
            self.user = int(self.lineEditUser.text())
        except ValueError:
            error("DNI ingresado erróneamente")
            return
        try:
            #: Busca usuario por DNI y devuelve el nombre, apellido y fecha de nacimiento
            nombre, apellido, fecha_nac = self.doc_user_o.buscar_usuario(user=self.user)
        except ValueError:
            error(msg="Usuario inexistente")
            self.lineEditNombre.setPlaceholderText("Nombre")
            self.lineEditApellido.setPlaceholderText("Apellido")
            self.dateNacimiento.setDate(QDate(1960, 1, 1))
            return
        self.lineEditNombre.setText(nombre)  #: Muestra el nombre en la línea de texto
        #: Muestra el apellido en la línea de texto
        self.lineEditApellido.setText(apellido)
        #: Muestra la fecha de nacimiento en la línea de texto
        self.dateNacimiento.setDate(QDate.fromString(fecha_nac, "dd/MM/yyyy"))

        for juego in get_game_names():
            #: Obtiene el nivel de cada juego para ese usuario
            nivel = self.doc_user_o.pedir_nivel(self.user, juego)
            self.QButtonGroup_d[juego].button(int(nivel[0])).setChecked(
                True
            )  #: Muestra el nivel en la interfaz gráfica

        self.buttonGroupBox.setDisabled(False)  #: Los botones comienzan deshabilitados
        self.dataGroupBox.setDisabled(
            False
        )  #: Los campos de datos del usuario comienzan deshabilitados
        self.gamesGroupBox.setDisabled(
            False
        )  #: Los campos de datos sobre la configuración de los juegos comienzan deshabilitados
        return

    def edit_user(self):
        """
        Función para editar el usuario
        """
        nombre = self.lineEditNombre.text()  #: Obtiene el nombre de la línea de texto
        apellido = (
            self.lineEditApellido.text()
        )  #: Obtiene el apellido de la línea de texto
        if (not nombre) or (not apellido):
            error(msg='"Nombre" y "Apellido" son campos obligatorios')
            return
        fecha = (
            self.dateNacimiento.text()
        )  #: Obtiene la fecha de nacimiento de la línea de texto

        juego_d = {}
        for juego in get_game_names():
            #: Diccionario con el juego y su nivel
            juego_d[juego] = str(
                self.QButtonGroup_d[juego].checkedId()
            )  #: Obtiene el nivel de cada juego de las líneas de texto

        #: Edita usuario en la base de datos
        if not self.doc_user_o.editar_usuario(
            self.user, nombre, apellido, fecha, juego_d
        ):
            error(msg="No se pudo editar el usuario")
        else:
            info(msg="Usuario editado con éxito")
            self.close()
