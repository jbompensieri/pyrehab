"""editar_regwindow.py 
Ventana en PyQT para editar un usuario profesional"""

from PyQt5.QtWidgets import *
from PyQt5 import QtGui
from pyqtutils import *
from doc_user import doc_user


class EditarRegWindow(QWidget):
    """Clase EditarRegWindow para ventana que permite editar los datos del usuario profesional"""

    def __init__(self):
        """
        Inicialización
        """
        super().__init__()
        self.doc_user_o = doc_user()
        self.createSearchGroupBox()
        self.createDataGroupBox()
        self.createButtonGroupBox()

        topLayout = QHBoxLayout()
        topLayout.addStretch(1)

        mainLayout = QGridLayout()
        mainLayout.addLayout(topLayout, 0, 0, 1, 2)
        mainLayout.addWidget(self.searchGroupBox, 1, 0)
        mainLayout.addWidget(self.dataGroupBox, 1, 1)
        mainLayout.addWidget(self.buttonGroupBox, 2, 0, 2, 0)
        mainLayout.setRowStretch(1, 1)
        mainLayout.setRowStretch(2, 1)
        mainLayout.setColumnStretch(0, 1)
        mainLayout.setColumnStretch(1, 1)
        self.setLayout(mainLayout)
        self.setWindowIcon(QtGui.QIcon(logo))
        self.setWindowTitle("Editar Registro")

        styleComboBox = QComboBox()
        styleComboBox.addItems(QStyleFactory.keys())
        styleComboBox.activated[str].connect(changeStyle)
        changeStyle()

    def createSearchGroupBox(self):
        """
        Cuadro para grupo de búsqueda de usuario por DNI
        """
        self.searchGroupBox = QGroupBox("Búsqueda de usuario")

        self.lineEditUser = QLineEdit()  #: Línea para escribir DNI del usuario
        self.lineEditUser.setPlaceholderText("DNI")

        self.buscarPushButton = QPushButton("Buscar")  #: Botón para buscar usuario
        self.buscarPushButton.setDefault(True)
        #: Conexión del botón con la función para buscar usuario
        self.buscarPushButton.clicked.connect(self.search_user)

        layout = QGridLayout()
        layout.addWidget(self.lineEditUser, 0, 0, 1, 1)
        layout.addWidget(self.buscarPushButton, 0, 1, 1, 2)
        layout.setRowStretch(5, 1)
        self.searchGroupBox.setLayout(layout)

    def createDataGroupBox(self):
        """
        Cuadro para mostrar los datos del usuario buscado
        """
        self.dataGroupBox = QGroupBox("Datos de usuario")

        self.lineEditNombre = QLineEdit()  #: Línea para editar nombre
        self.lineEditApellido = QLineEdit()  #: Línea para editar apellido
        self.lineEditContrasena = QLineEdit()  #: Línea para editar contraseña
        self.lineEditContrasena.setEchoMode(QLineEdit.Password)
        self.lineEditRepContrasena = QLineEdit()  #: Repetir contraseña por seguridad
        self.lineEditRepContrasena.setEchoMode(QLineEdit.Password)
        self.checkBoxAdmin = QCheckBox("Admin")

        label_1 = QLabel("Nombre")
        label_2 = QLabel("Apellido")
        label_3 = QLabel("Nueva Contraseña")
        label_4 = QLabel("Repetir Nueva Contraseña")

        layout = QGridLayout()
        layout.addWidget(label_1, 1, 0, 1, 1)
        layout.addWidget(label_2, 2, 0, 1, 1)
        layout.addWidget(label_3, 3, 0, 1, 1)
        layout.addWidget(label_4, 4, 0, 1, 1)

        layout.addWidget(self.lineEditNombre, 1, 1, 1, 2)
        layout.addWidget(self.lineEditApellido, 2, 1, 1, 2)
        layout.addWidget(self.lineEditContrasena, 3, 1, 1, 2)
        layout.addWidget(self.lineEditRepContrasena, 4, 1, 1, 2)
        layout.addWidget(self.checkBoxAdmin, 5, 0, 1, 2)
        layout.setRowStretch(5, 1)
        self.dataGroupBox.setLayout(layout)
        self.dataGroupBox.setDisabled(True)

    def createButtonGroupBox(self):
        """
        Cuadro con los botones
        """
        self.buttonGroupBox = QGroupBox()
        self.editarPushButton = QPushButton("Editar")
        self.editarPushButton.setDefault(False)
        self.cancelarPushButton = QPushButton("Cancelar")
        self.cancelarPushButton.setDefault(False)

        layout = QGridLayout()
        layout.addWidget(self.editarPushButton, 1, 0, 1, 2)
        layout.addWidget(self.cancelarPushButton, 2, 0, 1, 2)

        self.buttonGroupBox.setLayout(layout)
        #: Habilita los botones de Editar y Cancelar una vez ingresado correctamente el DNI del usuario a editar
        self.buttonGroupBox.setDisabled(True)

        self.editarPushButton.clicked.connect(
            self.edit_user
        )  #: Conexión del botón de edición
        self.cancelarPushButton.clicked.connect(
            self.close
        )  #: Conexión del botón de cancelar

    def search_user(self):
        """
        Función para buscar un profesional por DNI
        """
        try:
            #: self.user es el DNI ingresado en la línea de búsqueda
            self.user = int(self.lineEditUser.text())
        except ValueError:
            error("DNI ingresado erróneamente")
            return
        try:
            #: Busca usuario por DNI y devuelve el nombre, apellido y fecha de nacimiento
            nombre, apellido, is_admin = self.doc_user_o.buscar_profesional(
                user=self.user
            )
        except ValueError:
            error(msg="Usuario inexistente")
            self.lineEditNombre.setPlaceholderText("Nombre")
            self.lineEditApellido.setPlaceholderText("Apellido")
            self.lineEditContrasena.setPlaceholderText("Nueva Contraseña")
            self.lineEditContrasena.setPlaceholderText("Repetir Nueva Contraseña")
            return
        self.lineEditNombre.setText(nombre)  #: Muestra el nombre en la línea de texto
        self.lineEditApellido.setText(
            apellido
        )  #: Muestra el apellido en la línea de texto
        if is_admin:
            self.checkBoxAdmin.setChecked(True)

        self.buttonGroupBox.setDisabled(False)  #: Los botones comienzan deshabilitados
        self.dataGroupBox.setDisabled(
            False
        )  #: Los campos de datos del usuario comienzan deshabilitados
        return

    def edit_user(self):
        """
        Función para editar el profesional
        """
        nombre = self.lineEditNombre.text()  #: Obtiene el nombre de la línea de texto
        apellido = (
            self.lineEditApellido.text()
        )  #: Obtiene el apellido de la línea de texto
        contrasena = self.lineEditContrasena.text()
        repcontrasena = self.lineEditRepContrasena.text()

        if (not nombre) or (not apellido):
            error(msg='"Nombre" y "Apellido" son campos obligatorios')
            return

        if contrasena != repcontrasena:
            error(msg="Las contraseñas deben coincidir")
            return

        user_admin = 0
        if self.checkBoxAdmin.isChecked():
            user_admin = 1

        #: Edita usuario en la base de datos
        if not self.doc_user_o.editar_profesional(
            self.user, nombre, apellido, contrasena, user_admin
        ):
            error(msg="No se pudo editar el usuario")
        else:
            info(msg="Usuario editado con éxito")
            self.close()
