from PyQt5.QtWidgets import *
from PyQt5 import QtGui
from pyqtutils import *
from doc_user import doc_user


class RegistroWindow(QWidget):
    """Clase Registro Window para la ventana de agregar registro de profesional de salud en la base de datos"""

    def __init__(self):
        """
        Inicialización
        """
        super().__init__()
        self.doc_user_o = doc_user()
        self.createDataGroupBox()
        self.createButtonGroupBox()

        topLayout = QHBoxLayout()
        topLayout.addStretch(1)

        mainLayout = QGridLayout()
        mainLayout.addLayout(topLayout, 0, 0, 1, 1)
        mainLayout.addWidget(self.dataGroupBox, 1, 0)
        mainLayout.addWidget(self.buttonGroupBox, 2, 0, 2, 0)
        mainLayout.setRowStretch(1, 1)
        mainLayout.setRowStretch(2, 1)
        mainLayout.setColumnStretch(0, 1)
        self.setLayout(mainLayout)
        self.setWindowIcon(QtGui.QIcon(logo))
        self.setWindowTitle("Registrar Profesional")

        styleComboBox = QComboBox()
        styleComboBox.addItems(QStyleFactory.keys())
        styleComboBox.activated[str].connect(changeStyle)
        changeStyle()

    def createDataGroupBox(self):
        """
        Crea espacio para agregar los datos del profesional a registrar
        """
        self.dataGroupBox = QGroupBox("Datos")

        self.lineEditUser = QLineEdit()
        self.lineEditUser.setPlaceholderText("DNI")

        self.lineEditNombre = QLineEdit()
        self.lineEditNombre.setPlaceholderText("Nombre")

        self.lineEditApellido = QLineEdit()
        self.lineEditApellido.setPlaceholderText("Apellido")

        self.lineEditContrasena = QLineEdit()
        self.lineEditContrasena.setPlaceholderText("Contraseña")
        self.lineEditContrasena.setEchoMode(QLineEdit.Password)
        #: Por seguridad, se pide repetir la contraseña
        self.lineEditRepetirContrasena = QLineEdit()
        self.lineEditRepetirContrasena.setPlaceholderText("Repetir Contraseña")
        self.lineEditRepetirContrasena.setEchoMode(QLineEdit.Password)

        self.checkBoxAdmin = QCheckBox("Admin")

        layout = QGridLayout()
        layout.addWidget(self.lineEditUser, 0, 0, 1, 2)
        layout.addWidget(self.lineEditNombre, 1, 0, 1, 2)
        layout.addWidget(self.lineEditApellido, 2, 0, 1, 2)
        layout.addWidget(self.lineEditContrasena, 3, 0, 1, 2)
        layout.addWidget(self.lineEditRepetirContrasena, 4, 0, 1, 2)
        layout.addWidget(self.checkBoxAdmin, 5, 0, 1, 2)
        layout.setRowStretch(5, 1)
        self.dataGroupBox.setLayout(layout)

    def createButtonGroupBox(self):
        """
        Grupo de botones
        """
        self.buttonGroupBox = QGroupBox()

        self.aceptarPushButton = QPushButton("Aceptar")
        self.aceptarPushButton.setDefault(False)
        self.cancelarPushButton = QPushButton("Cancelar")
        self.cancelarPushButton.setDefault(False)

        layout = QGridLayout()
        layout.addWidget(self.aceptarPushButton, 1, 0, 1, 2)
        layout.addWidget(self.cancelarPushButton, 2, 0, 1, 2)

        self.buttonGroupBox.setLayout(layout)

        self.aceptarPushButton.clicked.connect(
            self.add_user
        )  #: Agrega usuario a la base de datos
        self.cancelarPushButton.clicked.connect(
            self.close
        )  #: Cancela la acción y cierra la ventana

    def add_user(self):
        """
        Función para agregar el nuevo profesional a la base de datos
        """
        if self.lineEditContrasena.text() == self.lineEditRepetirContrasena.text():
            #: Si ambas contraseñas ingresadas son iguales
            try:
                user = int(self.lineEditUser.text())
            except ValueError:
                error("DNI ingresado erróneamente")
                return
        else:
            error("Las contraseñas deben coincidir")
            return
        user = self.lineEditUser.text()
        user_n = self.lineEditNombre.text()
        user_a = self.lineEditApellido.text()
        user_pwd = self.lineEditContrasena.text()
        if (not user_n) or (not user_a):
            #: Nombre y apellido son campos obligatorios
            error(msg='"Nombre" y "Apellido" son campos obligatorios')
            return
        user_admin = False  #: Por default no es administrador
        if self.checkBoxAdmin.isChecked():
            user_admin = True  #: Es administrador si se selecciona la checkbox

        if not self.doc_user_o.registrar_profesional(
            user_data=(user, user_n, user_a, user_pwd, user_admin)
        ):
            error(msg="Usuario existente")
        else:
            info(msg="Usuario agregado con éxito")
            self.close()
