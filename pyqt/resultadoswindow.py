"""resultadoswindow.py 
Ventana en PyQT para extraer los resultados de un usuario a un archivo Excel"""
from PyQt5.QtWidgets import *
from PyQt5 import QtGui
from pyqtutils import *
import pandas as pd
from doc_user import doc_user


class ResultadosWindow(QWidget):
    """Clase Resultados Window para ventana que permite extraer datos de un usuario en un archivo Excel."""

    def __init__(self):
        """
        Inicialización
        """
        super().__init__()
        self.doc_user_o = doc_user()
        self.createSearchGroupBox()
        self.createDataGroupBox()
        self.createButtonGroupBox()

        topLayout = QHBoxLayout()
        topLayout.addStretch(1)

        mainLayout = QGridLayout()
        mainLayout.addLayout(topLayout, 0, 0, 1, 2)
        mainLayout.addWidget(self.searchGroupBox, 1, 0)
        mainLayout.addWidget(self.dataGroupBox, 1, 1)
        mainLayout.addWidget(self.buttonGroupBox, 2, 0, 2, 0)
        mainLayout.setRowStretch(1, 1)
        mainLayout.setRowStretch(2, 1)
        mainLayout.setColumnStretch(0, 1)
        mainLayout.setColumnStretch(1, 1)
        self.setLayout(mainLayout)
        self.setWindowIcon(QtGui.QIcon(logo))
        self.setWindowTitle("Resultados Usuario")

        styleComboBox = QComboBox()
        styleComboBox.addItems(QStyleFactory.keys())
        styleComboBox.activated[str].connect(changeStyle)
        changeStyle()

        self.dataGroupBox.setDisabled(True)  #: Campo inicia desactivado

    def createSearchGroupBox(self):
        """
        Cuadro para grupo de búsqueda de usuario por DNI
        """
        self.searchGroupBox = QGroupBox("Búsqueda de usuario")

        self.lineEditUser = QLineEdit()
        self.lineEditUser.setPlaceholderText("DNI")

        self.buscarPushButton = QPushButton("Buscar")
        self.buscarPushButton.setDefault(False)
        self.buscarPushButton.clicked.connect(
            self.search_user
        )  #: Conexión del botón de búsqueda

        layout = QGridLayout()
        layout.addWidget(self.lineEditUser, 0, 0, 1, 1)
        layout.addWidget(self.buscarPushButton, 0, 1, 1, 2)
        layout.setRowStretch(5, 1)
        self.searchGroupBox.setLayout(layout)

    def createDataGroupBox(self):
        """
        Cuadro para mostrar los datos del usuario buscado
        """
        self.dataGroupBox = QGroupBox("Datos del usuario")

        self.lineEditNombre = QLineEdit()
        self.lineEditApellido = QLineEdit()
        self.lineEditFechaNac = QLineEdit()

        layout = QGridLayout()
        layout.addWidget(self.lineEditNombre, 1, 0, 1, 2)
        layout.addWidget(self.lineEditApellido, 2, 0, 1, 2)
        layout.addWidget(self.lineEditFechaNac, 3, 0, 1, 2)

        self.dataGroupBox.setLayout(layout)

    def createButtonGroupBox(self):
        """
        Cuadro para mostrar los botones
        """
        self.buttonGroupBox = QGroupBox()
        self.extraerPushButton = QPushButton("Extraer a archivo")
        self.extraerPushButton.setDefault(False)
        self.cancelarPushButton = QPushButton("Cancelar")
        self.cancelarPushButton.setDefault(False)

        layout = QGridLayout()
        layout.addWidget(self.extraerPushButton, 1, 0, 1, 2)
        layout.addWidget(self.cancelarPushButton, 2, 0, 1, 2)

        self.buttonGroupBox.setLayout(layout)
        self.buttonGroupBox.setDisabled(True)

        self.extraerPushButton.clicked.connect(
            self.results_user
        )  #: Conexión del botón de extraer datos del usuario

        self.cancelarPushButton.clicked.connect(
            self.close
        )  #: Conexión del botón cancelar

    def search_user(self):
        """
        Función para buscar usuario por DNI
        """
        try:
            user = int(self.lineEditUser.text())
        except ValueError:
            error("DNI ingresado erróneamente")
            return
        self.buttonGroupBox.setDisabled(False)  #: Habilita los botones
        try:
            nombre, apellido, fecha_nac = self.doc_user_o.buscar_usuario(
                user=user
            )  #: Busca el usuario por DNI
        except ValueError:
            error(msg="Usuario inexistente")
            self.lineEditNombre.setPlaceholderText("Nombre")
            self.lineEditApellido.setPlaceholderText("Apellido")
            self.lineEditFechaNac.setPlaceholderText("Fecha de nacimiento")
            return
        self.lineEditNombre.setPlaceholderText(nombre)
        self.lineEditApellido.setPlaceholderText(apellido)
        self.lineEditFechaNac.setPlaceholderText(fecha_nac)

    def results_user(self):
        """
        Función para extraer datos del usuario desde la base de datos en MySQL a un archivo Excel
        """
        try:
            #: self.user es el DNI ingresado en la línea de búsqueda
            user = int(self.lineEditUser.text())
        except ValueError:
            error("DNI ingresado erróneamente")
            return
        try:
            #: Busca usuario por DNI y devuelve el nombre, apellido y fecha de nacimiento
            plugin_l = get_game_names()
            resdf_d = {}
            #: Permite seleccionar la carpeta donde se va a guardar el archivo
            file_path = str(
                QFileDialog.getExistingDirectory(self, "Seleccione carpeta")
            )
            if not file_path:
                #: Si no selecciona carpeta (porque cerró la ventana) no guarda el archivo
                return
            file_name = f"{user}.xlsx"  #: El nombre del archivo es el DNI del jugador
            with pd.ExcelWriter(os.path.join(file_path, file_name)) as writer:
                for juego in plugin_l:
                    resdf_d[juego] = self.doc_user_o.buscar_resultados(
                        user=user, game=juego
                    )
                    resdf_d[juego].to_excel(
                        writer,
                        sheet_name=juego,
                        columns=[
                            "dni",
                            "nivel",
                            "cant_aciertos",
                            "cant_errores",
                            "tiempo",
                            "fecha",
                        ],
                    )
                    #: Ancho de las columnas para que se pueda leer correctamente el encabezado
                    workbook = writer.book
                    worksheet = writer.sheets[juego]
                    (max_row, max_col) = resdf_d[juego].shape
                    worksheet.set_column(1, max_col, 20)
            info("Archivo creado exitosamente")
        except ValueError:
            error(msg="Usuario inexistente")
