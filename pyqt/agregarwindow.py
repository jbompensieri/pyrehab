"""agregarwindow.py 
Ventana en PyQT para agregar un usuario"""

from PyQt5.QtWidgets import *
from PyQt5 import QtGui
from pyqtutils import *
from PyQt5.QtCore import QDate
from doc_user import doc_user


class AgregarWindow(QWidget):
    """
    Clase AgregarWindow para ventana que permite agregar un usuario."""

    def __init__(self):
        """
        Inicialización
        """
        super().__init__()
        self.doc_user_o = doc_user()  #: Objeto clase doc_user
        self.createDataGroupBox()
        self.createGamesGroupBox()
        self.createButtonGroupBox()

        topLayout = QHBoxLayout()
        topLayout.addStretch(1)

        mainLayout = QGridLayout()
        mainLayout.addLayout(topLayout, 0, 0, 1, 2)
        mainLayout.addWidget(self.dataGroupBox, 1, 0)  #: Widget para el grupo de datos
        mainLayout.addWidget(
            self.gamesGroupBox, 1, 1
        )  #: Widget para el grupo de juegos
        mainLayout.addWidget(self.buttonGroupBox, 2, 0, 2, 0)  #: Widget para botones
        mainLayout.setRowStretch(1, 1)
        mainLayout.setRowStretch(2, 1)
        mainLayout.setColumnStretch(0, 1)
        mainLayout.setColumnStretch(1, 1)
        self.setLayout(mainLayout)
        self.setWindowIcon(QtGui.QIcon(logo))
        self.setWindowTitle("Agregar Usuario")

        styleComboBox = QComboBox()
        styleComboBox.addItems(QStyleFactory.keys())
        styleComboBox.activated[str].connect(changeStyle)
        changeStyle()

    def createDataGroupBox(self):
        """
        Cuadro para introducir los datos del usuario en cada QLineEdit
        """
        self.dataGroupBox = QGroupBox("Datos de usuario")

        self.lineEditUser = QLineEdit()
        self.lineEditUser.setPlaceholderText("DNI")

        self.lineEditNombre = QLineEdit()
        self.lineEditNombre.setPlaceholderText("Nombre")

        self.lineEditApellido = QLineEdit()
        self.lineEditApellido.setPlaceholderText("Apellido")

        self.dateNacimiento = QDateEdit(self.dataGroupBox)
        self.dateNacimiento.setDisplayFormat("dd/MM/yyyy")
        self.dateNacimiento.setDate(QDate(1960, 1, 1))

        layout = QGridLayout()
        layout.addWidget(self.lineEditUser, 0, 0, 1, 2)
        layout.addWidget(self.lineEditNombre, 1, 0, 1, 2)
        layout.addWidget(self.lineEditApellido, 2, 0, 1, 2)
        layout.addWidget(self.dateNacimiento, 3, 0, 1, 2)
        layout.setRowStretch(5, 1)
        self.dataGroupBox.setLayout(layout)

    def createGamesGroupBox(self):
        """
        Cuadro para introducir el nivel de cada juego
        """
        self.gamesGroupBox = QGroupBox("Juegos")
        self.QButtonGroup_d = {}
        plugin_l = get_game_names()  #: Lista con nombre de los plugins o juegos
        layout = QGridLayout()
        i = 0

        for juego in plugin_l:
            self.QButtonGroup_d[juego] = QButtonGroup(self)
            rb1 = QRadioButton("1", self)
            rb2 = QRadioButton("2", self)
            rb3 = QRadioButton("3", self)
            self.QButtonGroup_d[juego].addButton(rb1)
            self.QButtonGroup_d[juego].setId(rb1, 1)
            self.QButtonGroup_d[juego].addButton(rb2)
            self.QButtonGroup_d[juego].setId(rb2, 2)
            self.QButtonGroup_d[juego].addButton(rb3)
            self.QButtonGroup_d[juego].setId(rb3, 3)
            self.QButtonGroup_d[juego].button(1).setChecked(True)
            label = QLabel(f"{juego}")
            layout.addWidget(label, i, 0, 1, 2)
            i += 1
            layout.addWidget(rb1, i, 0, 1, 2)
            layout.addWidget(rb2, i, 1, 1, 2)
            layout.addWidget(rb3, i, 2, 1, 2)
            i += 1

        layout.setRowStretch(i - 1, 1)
        self.gamesGroupBox.setLayout(layout)

    def createButtonGroupBox(self):
        """
        Cuadro con los botones para ingresar al usuario o cancelar la acción
        """
        self.buttonGroupBox = QGroupBox()

        self.aceptarPushButton = QPushButton("Aceptar")
        self.aceptarPushButton.setDefault(False)
        self.cancelarPushButton = QPushButton("Cancelar")
        self.cancelarPushButton.setDefault(False)

        layout = QGridLayout()
        layout.addWidget(self.aceptarPushButton, 1, 0, 1, 2)
        layout.addWidget(self.cancelarPushButton, 2, 0, 1, 2)

        self.buttonGroupBox.setLayout(layout)

        self.aceptarPushButton.clicked.connect(self.add_user)
        self.cancelarPushButton.clicked.connect(self.close)

    def add_user(self):
        """
        Función para agregar usuario
        """
        try:
            user = int(self.lineEditUser.text())  #: DNI del usuario
        except ValueError:
            error("DNI ingresado erróneamente")
            return
        user_n = self.lineEditNombre.text()  #: Nombre del usuario
        user_a = self.lineEditApellido.text()  #: Apellido del usuario
        user_f = self.dateNacimiento.text()  #: Fecha de nacimiento del usuario
        if (not user_n) or (not user_a):
            error(msg='"Nombre" y "Apellido" son campos obligatorios')
            return
        juegos_d = {}
        for juego in get_game_names():
            #: Diccionario con el juego y su nivel
            juegos_d[juego] = str(self.QButtonGroup_d[juego].checkedId())
        #: Agrega usuario en la base de datos. No permite ingresar dos veces el mismo usuario.
        if not self.doc_user_o.agregar_usuario(
            user_data=(user, user_n, user_a, user_f, juegos_d)
        ):
            error(msg="Usuario existente")
        else:
            info(msg="Usuario agregado con éxito")
            self.close()
