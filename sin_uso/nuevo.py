"""inputbox.py 
Pantalla de ingreso del jugador con su dni.
"""
import pygame, pygame.font, pygame.event, pygame.draw, string
from pygame.locals import *
from utils import *
import sys

def main(disp="Name", x=600, y=400):
    """
    Main. Define dimensiones de la pantalla y mensaje a escribir.
    : param disp: Mensaje a escribir
    : param x: Ancho de la pantalla
    : param y: Largo de la pantalla
    : return: Mensaje en la pantalla y texto que escribe el usuario
    """
    screen = pygame.display.set_mode((x, y))
    screen.fill(lightgreen_unsam)
    #: Fuentes
    pygame.font.init()
    TitFont = pygame.font.Font(fuente, 40)
    InstFont = pygame.font.Font(fuente, 25)
    DNIFont = pygame.font.Font(fuente, 30)
    #: Título
    title = TitFont.render("PyRehab", True, green_unsam)
    screen.blit(title, (10, 20))
    #: Instrucciones
    instr = "Ingrese el DNI y luego presione Entrar"
    blit_text(screen, instr, (10, 100), InstFont, black)
    #: Cuadro de texto
    input_box = pygame.Rect(200, 180, 200, 32)
    pygame.draw.rect(screen, green_unsam, input_box, 2)
    #: Botón de ingreso
    boton = pygame.Rect(0, (y - 50), x, 50)
    pygame.draw.rect(screen, green_unsam, boton)
    screen.blit(InstFont.render("Entrar", True, black), (250, 350))
    # #: Loop
    done = False
    text = ""
    while not done:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                # login = True
                sys.exit(0)
            elif event.type == pygame.MOUSEBUTTONDOWN:  # si se hace click
                if boton.collidepoint(event.pos):
                    done = True
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RETURN:
                    done = True
                elif event.key == pygame.K_BACKSPACE:
                    text = text[0:-1]
                else:
                    if event.unicode not in "0123456789":
                        continue
                    if len(text) > 9:
                        continue
                    text += event.unicode

        pygame.draw.rect(screen, green_unsam, input_box, width=0)
        txt_surface = DNIFont.render(text, True, black)
        screen.blit(txt_surface, (input_box.x + 5, input_box.y - 5))
        pygame.display.update()
    return print(text)


if __name__ == "__main__":
    main()
