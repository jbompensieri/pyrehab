"""admin.py 
Funciones para las acciones que puede realizar el usuario administrador.
Este usuario es el encargado de agregar, editar y/o eliminar los registros de los profesionales 
que pueden acceder a la aplicacion."""

from getpass import getpass
from utils import data_dir
from pyrehab_crypt import pyrehab_crypt
import sys
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "sql")))
from pyrehab_sql import pyrehab_sql


class admin:
    """Clase admin que va a contener las funciones de las acciones permitidas para el usuario administrador."""

    def __init__(self):
        """
        Inicialización
        """
        self.c = pyrehab_crypt()
        self.sql = pyrehab_sql()
        self.table_name = "prof_table"  #: Nombre de la tabla en SQL
        self.login()
        self.run()

    def login(self):
        """
        Ingreso del usuario administrador.
        Si no ingresa la contraseña correcta en 3 intentos, se cierra el programa.
        """
        msg = "Ingrese admin password"
        cond = "nombre = 'admin' AND apellido = 'admin'"
        [(pwd,)] = self.sql.select_from_table(
            self.table_name, columns_l="password", condition=cond
        )  #: Contraseña almacenada en la tabla de SQL
        for i in range(3):
            if getpass(msg) != self.c.decrypt(
                pwd
            ):  #: Compara contraseña ingresada y almacenada
                print("\nContraseña incorrecta")
                if i == 2:
                    print("Saliendo del programa.")
                else:
                    print("Intente nuevamente.")
            else:
                print("\nContraseña correcta\n")
                return
        sys.exit(1)

    def run(self):
        """
        Retorna el menú con las opciones para el usuario administrador.
        """
        menu = """
        0. Salir
        1. Agregar médico
        2. Eliminar médico
        3. Editar médico
        """
        op = 1
        while op != 0:
            try:
                op = int(input(menu))
            except ValueError:
                continue
            if op == 1:
                self.agregar_medico()
            elif op == 2:
                self.eliminar_medico()
            elif op == 3:
                self.editar_medico()
            elif op != 0:
                print("Opción incorrecta")
        sys.exit(0)

    def agregar_medico(self):
        """
        Función para registrar profesional de salud.
        Crea una nueva fila en la tabla de SQL con los datos del profesional (DNI, nombre, apellido, contraseña).
        """
        msg = "Ingrese DNI del médico\t"
        dni = input(msg)  #: Ingresa DNI del profesional
        msg = "Ingrese nombre del médico\t"
        nom = input(msg)  #: Ingresa nombre del profesional
        msg = "Ingrese apellido del médico\t"
        ape = input(msg)  #: Ingresa apellido del profesional
        msg = "Ingrese contraseña del médico\t"
        pwd = getpass(msg)  #: Ingresa contraseña del profesional
        msg = "Ingrese la misma contraseña nuevamente"
        if pwd != getpass(msg):  #:Compara contraseñas por seguridad
            print(f"ERROR: Las contraseñas no coinciden")
            return
        pwd_crypted = self.c.encrypt(pwd)  #: Encripta la contraseña del profesional
        is_ok = self.sql.add_row(
            self.table_name, (dni, nom, ape, pwd_crypted.decode("utf-8"))
        )  #: Agrega la fila a la tabla de SQL
        if is_ok:
            print(f"Info: Médico {ape} agregado con éxito")
        else:
            print("El DNI ingresado ya existe")

    def eliminar_medico(self):
        """
        Función para eliminar profesional de salud.
        Elinina la fila en la tabla de SQL según el DNI del profesional.
        """
        msg = "Ingrese DNI del médico a eliminar\t"
        dni = input(msg)  #: Ingresa DNI del usuario a eliminar
        print(f"Confirma que desea eliminar el DNI: {dni}?")
        print("\tEsta acción es irreversible")
        op = input("SI/NO\n")  #: Confirmación previa a la eliminación
        if op.lower() == "si" or op.lower() == "s":
            self.sql.delete_row(
                self.table_name, (f"dni = {dni}")
            )  #: Elimina la fila de la tabla
            print(f"Info: DNI {dni} eliminado con éxito")
        else:
            print(f"Info: Operación cancelada")

    def editar_medico(self):
        """
        Función para editar registro del profesional de salud.
        Edita las columnas de la fila en la tabla de SQL según el DNI del profesional.
        """
        print("Info: Si no desea editar el campo, pulse la tecla enter\n")
        msg = "Ingrese DNI del médico a editar\t"
        dni = input(msg)
        command = f"UPDATE {self.table_name} SET "  #: Rimera parte del comando de update tabla
        msg = "Ingrese nuevo nombre\t"
        nom = input(msg)
        if nom:
            command += (
                f"nombre = '{nom}', "  #: Si desea modificar el nombre, agrega comando
            )
        msg = "Ingrese nuevo apellido\t"
        ape = input(msg)
        if ape:
            command += f"apellido = '{ape}', "  #: Si desea modificar el apellido, agrega comando
        msg = "Ingrese la nueva contraseña\t"
        pwd = getpass(msg)
        if pwd:
            msg = "Ingrese la misma contraseña nuevamente"
            if pwd != getpass(msg):
                print(f"ERROR: Las contraseñas no coinciden")
                return
            pwd_crypted = self.c.encrypt(pwd)
            pwd = pwd_crypted.decode("utf-8")
            if pwd:
                command += f"password = '{pwd}'"  #: Si desea modificar la contraseña, agrega comando
        if command[-2:] == ", ":
            command = command[:-2]
        command += (
            f" WHERE dni = {dni}"  #: Condicion del comando para buscar fila por dni
        )
        self.sql.edit_row(command)
        msg = "Médico editado correctamente"


if __name__ == "__main__":
    admin_o = admin()
