"""inputbox.py 
Pantalla de ingreso del jugador con su dni.
"""
import pygame, pygame.font, pygame.event, pygame.draw, string
from pygame.locals import *
from utils import *


def getk():
    """
    Devuelve los eventos de teclas presionadas.
    : return: Tecla presionada
    """
    while 1:
        event = pygame.event.poll()
        if event.type == KEYDOWN:
            return event.key
        else:
            pass


def display_box(screen, message):
    """
    Imprime un mensaje en el centro de la pantalla dentro de un rectángulo negro.
    : param screen: Pantalla
    : param message: Mensaje a escribir
    """
    fontobject = pygame.font.Font(None, 18)  #: Defino la fuente del texto
    #: Dibuja un rectángulo negro en el centro de la pantalla para el mensaje
    pygame.draw.rect(
        screen,
        (0, 0, 0),
        ((screen.get_width() / 2) - 100, (screen.get_height() / 2) - 10, 200, 20),
        0,
    )
    #: Dibuja un rectángulo blanco en el centro de la pantalla para el cuadro de texto. Está defasado al rectángulo anterior
    pygame.draw.rect(
        screen,
        (255, 255, 255),
        ((screen.get_width() / 2) - 35, (screen.get_height() / 2) - 12, 204, 24),
        1,
    )
    if len(message) != 0:
        screen.blit(
            fontobject.render(message, 1, (255, 255, 255)),
            ((screen.get_width() / 2) - 250, (screen.get_height() / 2) - 10),
        )  #: Muestra el texto en la pantalla, en el rectángulo negro
    pygame.display.update()  #: Actualiza pantalla


def ask(screen, question):
    """
    Utiliza display_box para imprimir el mensaje en la pantalla y lo que se está escribiendo en el espacio de texto.
    : param screen: Pantalla
    : param question: Mensaje a escribir
    : return: String que escribe el usuario
    """
    pygame.font.init()
    current_string = ""  #: String inicial vacío
    display_box(screen, question + ": " + current_string)
    while 1:
        inkey = getk()  #: Eventos de las teclas presionadas
        if inkey == K_BACKSPACE:  #: Si presiona tecla para borrar
            current_string = current_string[0:-1]
        elif inkey == K_RETURN:  #: Si presiona enter
            break
        elif inkey == K_MINUS:  #: Si presiona el símbolo menos
            current_string += "_"
        elif inkey <= 127:  #: Si presiona cualquier otra tecla de caracteres
            current_string += chr(inkey)  #: Agrega el caracter al string
        display_box(
            screen, question + ": " + current_string
        )  #: Muestra en pantalla el mensaje y el string que se está escribiendo
    return current_string


def main(disp="Name", x=320, y=240):
    """
    Main. Define dimensiones de la pantalla y mensaje a escribir.
    : param disp: Mensaje a escribir
    : param x: Ancho de la pantalla
    : param y: Largo de la pantalla
    : return: Mensaje en la pantalla y texto que escribe el usuario
    """
    screen = pygame.display.set_mode((x, y))
    return ask(screen, disp)


if __name__ == "__main__":
    main()
