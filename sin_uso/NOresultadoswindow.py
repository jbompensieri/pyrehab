# from PyQt5.QtWidgets import *
# from PyQt5 import QtGui
# from pyqtutils import *
# from PyQt5.QtCore import QDate
# import sys
# import os
# from doc_user import doc_user
# from utils import *
# import numpy as np
# import pyqtgraph as pg


# class ResultadosWindow(QWidget):
#     """
#     This "window" is a QWidget. If it has no parent, it
#     will appear as a free-floating window as we want.
#     """

#     def __init__(self):
#         super().__init__()
#         self.doc_user_o = doc_user()
#         self.createDataGroupBox()
#         self.createResultGroupBox()
#         self.createButtonGroupBox()

#         topLayout = QHBoxLayout()
#         topLayout.addStretch(1)

#         mainLayout = QGridLayout()
#         mainLayout.addLayout(topLayout, 0, 0, 1, 2)
#         mainLayout.addWidget(self.dataGroupBox, 1, 0)
#         mainLayout.addWidget(self.resultGroupBox, 1, 1)
#         mainLayout.addWidget(self.buttonGroupBox, 2, 0, 2, 1)
#         mainLayout.setRowStretch(1, 1)
#         mainLayout.setRowStretch(2, 1)
#         mainLayout.setColumnStretch(0, 1)
#         mainLayout.setColumnStretch(1, 1)
#         self.setLayout(mainLayout)
#         self.setWindowIcon(QtGui.QIcon(logo))
#         self.setWindowTitle("Resultados de Usuario")

#         styleComboBox = QComboBox()
#         styleComboBox.addItems(QStyleFactory.keys())
#         styleComboBox.activated[str].connect(self.changeStyle)
#         self.changeStyle()

#     def changeStyle(self, styleName="windowsvista"):
#         QApplication.setStyle(QStyleFactory.create(styleName))
#         QApplication.setPalette(QApplication.style().standardPalette())

#     def createDataGroupBox(self):
#         self.dataGroupBox = QGroupBox("Datos del usuario")

#         self.lineEditUser = QLineEdit()
#         self.lineEditUser.setPlaceholderText("DNI")

#         self.buscarPushButton = QPushButton("Buscar")
#         self.buscarPushButton.setDefault(True)
#         self.buscarPushButton.clicked.connect(self.search_user)

#         self.lineEditNombre = QLineEdit()
#         self.lineEditApellido = QLineEdit()
#         self.dateNacimiento = QDateEdit(self.dataGroupBox)
#         self.dateNacimiento.setDisplayFormat("dd/MM/yyyy")

#         layout = QGridLayout()
#         label_1 = QLabel("Nombre")
#         label_2 = QLabel("Apellido")
#         label_3 = QLabel("Fecha de nac.")
#         layout.addWidget(self.lineEditUser, 0, 0, 1, 1)
#         layout.addWidget(self.buscarPushButton, 0, 1, 1, 2)
#         layout.addWidget(label_1, 1, 0, 1, 1)
#         layout.addWidget(label_2, 2, 0, 1, 1)
#         layout.addWidget(label_3, 3, 0, 1, 1)
#         layout.addWidget(self.lineEditNombre, 1, 1, 1, 2)
#         self.lineEditNombre.setEnabled(False)
#         layout.addWidget(self.lineEditApellido, 2, 1, 1, 2)
#         self.lineEditApellido.setEnabled(False)
#         layout.addWidget(self.dateNacimiento, 3, 1, 1, 2)
#         self.dateNacimiento.setEnabled(False)
#         layout.setRowStretch(5, 1)
#         self.dataGroupBox.setLayout(layout)

#     def createButtonGroupBox(self):
#         """
#         Cuadro con los botones
#         """
#         self.buttonGroupBox = QGroupBox()
#         self.QPushButton_d = {}
#         layout = QGridLayout()

#         plugin_l = get_plugin_names()
#         i = 0
#         for juego in plugin_l:
#             self.QPushButton_d[juego] = QPushButton(juego, self)
#             layout.addWidget(self.QPushButton_d[juego], i, 0, 1, 2)
#             print(juego)
#             i += 1
#         layout.setRowStretch(i - 1, 1)
#         self.buttonGroupBox.setLayout(layout)
#         print("diccionario", self.QPushButton_d)
#         for key in self.QPushButton_d.keys():
#             button = self.QPushButton_d[key]
#             # self.QPushButton_d[key].clicked.connect(lambda: self.get_info(key))
#             button.clicked.connect(lambda state, x=key: self.get_info(key))
#             print("for ", key, button)

#     def createResultGroupBox(self):
#         self.resultGroupBox = QGroupBox("Gráficos")
#         layout = QGridLayout()
#         my_plot = pg.PlotWidget()
#         layout.addWidget(my_plot)
#         x = np.arange(-3, 3, 0.5, dtype=int)
#         y = x
#         my_plot.plot(x, y)
#         self.resultGroupBox.setLayout(layout)

#     def search_user(self):
#         """
#         Función para buscar un usuario por DNI
#         """
#         try:
#             #: self.user es el DNI ingresado en la línea de búsqueda
#             self.user = int(self.lineEditUser.text())
#         except ValueError:
#             error("DNI ingresado erróneamente")
#             return
#         try:
#             #: Busca usuario por DNI y devuelve el nombre, apellido y fecha de nacimiento
#             nombre, apellido, fecha_nac = self.doc_user_o.buscar_usuario(user=self.user)
#         except ValueError:
#             error(msg="Usuario inexistente")
#             self.lineEditNombre.setPlaceholderText("Nombre")
#             self.lineEditApellido.setPlaceholderText("Apellido")
#             self.dateNacimiento.setDate(QDate(1960, 1, 1))
#             return
#         # res = self.doc_user_o.buscar_resultados(user=self.user, game="memotest")

#         self.lineEditNombre.setText(nombre)  #: Muestra el nombre en la línea de texto
#         #: Muestra el apellido en la línea de texto
#         self.lineEditApellido.setText(apellido)
#         #: Muestra la fecha de nacimiento en la línea de texto
#         self.dateNacimiento.setDate(QDate.fromString(fecha_nac, "dd/MM/yyyy"))

#     def get_info(self, game):
#         """
#         Función para obtener y graficar los resultados del juego seleccionado
#         """
#         # try:
#         #     #: self.user es el DNI ingresado en la línea de búsqueda
#         #     self.user = int(self.lineEditUser.text())
#         # except ValueError:
#         #     error("DNI ingresado erróneamente")
#         #     return
#         # try:
#         #     #: Busca usuario por DNI y devuelve el nombre, apellido y fecha de nacimiento
#         #     res_df = self.doc_user_o.buscar_resultados(user=self.user, game=game)
#         #     print(res_df)
#         # except ValueError:
#         #     error(msg="Usuario inexistente")
#         print("funcion ", self.QPushButton_d[game], game)
