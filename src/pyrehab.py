"""pyrehab.py 
Ejecuta la aplicación PyRehab.
"""
from model import model
from view import view
import sys
import os
from utils import *

os.environ["SDL_VIDEO_WINDOW_POS"] = "%d,%d" % (50, 50)


class pyrehab:
    """Clase pyrehab que permite el ingreso y ejecución de los juegos."""

    def __init__(self):
        """
        Inicialización
        """
        self.model = model()  #: Clase model
        self.game_l = (
            self.model.plug_l
        )  #: Lista de juegos en la carpeta plugins (objetos)
        self.game_names_l = get_plugin_names()
        self.view = view(self.game_l)  #: Clase view
        # print(self.model.plug_l)
        self.run()

    def run(self):
        """
        Ejecuta PyRehab si el ingreso es correcto
        """
        login = False
        while not login:
            #: Muestra pantalla de ingreso a los juegos y obtiene el dni ingresado por el jugador
            dni = self.view.show_login()
            if not dni:
                sys.exit(0)
            try:
                dni = int(dni)
            except ValueError:
                continue
            if self.model.login(user=dni):
                #: Si el login fue correcto, se configuran los juegos según información en la base de datos para ese usuario en particular
                for game in self.game_names_l:
                    for g in self.game_l:
                        n = int(self.model.config_juego(user=dni, game=game))
                        if g.name == game:
                            g.set_nivel(n)
                            g.set_user(dni)
                            break
                login = True
        self.view.show_menu()  #: Muestra pantalla de menú de los juegos


pyrehab_o = pyrehab()
