"""model.py 
Archivo encargado de la conexión con la base de datos desde PyRehab jugador y también con la carpeta Plugins
"""
import sys
import os
import importlib
from utils import *

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "sql")))
from pyrehab_sql import pyrehab_sql


class model:
    """Clase model, encargada de los plugins y guardar datos."""

    def __init__(self):
        """
        Inicialización
        """
        self.plug_l = self.get_plugin_objs(
            plugin_name_l=get_plugin_names()
        )  #: Lista de plugins (objetos)
        self.db = pyrehab_sql()  #: Objeto para uso de sql
        self.table_name = "user_table"

    def get_plugin_objs(self, plugin_name_l):
        """Función para obtener una lista de objetos de los plugins
        : param plugin_name_l: Lista con los nombres de los plugins
        : return: Lista con objetos de cada plugin
        """
        plug_l = []
        # msg = "Import plugins"
        # print(f"INFO: {msg}")
        for p in plugin_name_l:
            # msg = f"Import plugin {p}"
            # print(f"INFO: {msg}")
            plugpath = f"{plugin_dirname}.{p}"
            plug = importlib.import_module(plugpath)
            plug_class = getattr(plug, p)
            try:
                plug_l.append(plug_class(name=p))
            except TypeError:
                msg = (
                    f"Plugin {p} no implementa uno o más de los "
                    "métodos obligatorios.\n\tPor favor ver "
                    "https://gitlab.com/jbompensieri/"
                    "pyrehab/-/blob/main/README.md"
                )
                print(f"ERROR: {msg}")
                sys.exit(1)
            except Exception as e:
                msg = f"Exception {e} raised when trying to " f"instantiate plugin {p}."
                print(f"ERROR: {msg}")
                sys.exit(1)
        return plug_l

    def login(self, user):
        """
        Ingreso del jugador.
        : param user: DNI del jugador
        : return: True si el jugador existe en la base de datos
        """
        cond = f"dni = '{user}'"
        if not self.db.select_from_table(
            self.table_name, columns_l="nombre", condition=cond
        ):
            return False
        return True

    def config_juego(self, user, game):
        """
        Configuración de los niveles de los juegos
        : param user: DNI del jugador
        : param game: juego a configurar
        : return: nivel del juego, guardado en la base de datos
        """
        cond = f"dni = '{user}'"
        [(nivel,)] = self.db.select_from_table(
            self.table_name, columns_l=game, condition=cond
        )
        return nivel

    def load_data(self, user_data, game):
        """
        Carga los datos extraidos de los juegos en la base de datos en SQL
        : param user_data: información del usuario (dni y parámetros)
        : param game: nombre del juego, será el nombre de la tabla
        """
        self.db.add_row(
            table_name=game,
            row_values=user_data,
            columns="(dni, nivel, cant_aciertos, cant_errores, tiempo, fecha)",
        )
