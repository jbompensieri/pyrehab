# -*- mode: python ; coding: utf-8 -*-


block_cipher = None

added_files = [
         ( 'D:\\Proyecto Final\\PyRehab\\src\\assets\\*.png', 'assets' ),
         ( 'D:\\Proyecto Final\\PyRehab\\src\\assets\\*.ttf', 'assets' ),
         ( 'D:\\Proyecto Final\\PyRehab\\src\\assets\\diferencias\\dificil\\*.png', 'assets\\diferencias\\dificil' ),
         ( 'D:\\Proyecto Final\\PyRehab\\src\\assets\\diferencias\\dificil\\*.yml', 'assets\\diferencias\\dificil' ),
         ( 'D:\\Proyecto Final\\PyRehab\\src\\assets\\diferencias\\facil\\*.png', 'assets\\diferencias\\facil' ),
         ( 'D:\\Proyecto Final\\PyRehab\\src\\assets\\diferencias\\facil\\*.yml', 'assets\\diferencias\\facil' ),
         ( 'D:\\Proyecto Final\\PyRehab\\src\\assets\\diferencias\\medio\\*.png', 'assets\\diferencias\\medio' ),
         ( 'D:\\Proyecto Final\\PyRehab\\src\\assets\\diferencias\\medio\\*.yml', 'assets\\diferencias\\medio' ),
         ( 'D:\\Proyecto Final\\PyRehab\\src\\assets\\escalas\\*.png', 'assets\\escalas' ),
         ( 'D:\\Proyecto Final\\PyRehab\\src\\assets\\memotest\\argentina\\*.png', 'assets\\memotest\\argentina' ),
         ( 'D:\\Proyecto Final\\PyRehab\\src\\assets\\memotest\\banderas\\*.png', 'assets\\memotest\\banderas' ),
         ( 'D:\\Proyecto Final\\PyRehab\\src\\assets\\memotest\\monedas\\*.png', 'assets\\memotest\\monedas' ),
         ( 'D:\\Proyecto Final\\PyRehab\\src\\assets\\memotest\\monumentos\\*.png', 'assets\\memotest\\monumentos' ),
         ( 'D:\\Proyecto Final\\PyRehab\\src\\assets\\sonidos\\*.wav', 'assets\\sonidos' ),
         ( 'D:\\Proyecto Final\\PyRehab\\src\\data\\crypt.key', 'data' ),
         ( 'D:\\Proyecto Final\\PyRehab\\src\\Plugins\\*.py', 'Plugins' )
         ]

a = Analysis(
    ['pyrehab.py'],
    pathex=['D:\\Proyecto Final\\PyRehab\\src', 'D:\\Proyecto Final\\PyRehab\\src\\Plugins',
            'D:\\Proyecto Final\\PyRehab\\venv\\Lib\\site-packages'],
    binaries=[],
    datas=added_files,
    hiddenimports=[],
    hookspath=[],
    hooksconfig={},
    runtime_hooks=[],
    excludes=[],
    win_no_prefer_redirects=False,
    win_private_assemblies=False,
    cipher=block_cipher,
    noarchive=False,
)
pyz = PYZ(a.pure, a.zipped_data, cipher=block_cipher)

exe = EXE(
    pyz,
    a.scripts,
    [],
    exclude_binaries=True,
    name='pyrehab',
    debug=False,
    bootloader_ignore_signals=False,
    strip=False,
    upx=True,
    console=True,
    disable_windowed_traceback=False,
    argv_emulation=False,
    target_arch=None,
    codesign_identity=None,
    entitlements_file=None,
)
coll = COLLECT(
    exe,
    a.binaries,
    a.zipfiles,
    a.datas,
    strip=False,
    upx=True,
    upx_exclude=[],
    name='pyrehab',
)




pyinstaller --noconfirm --onedir --console --add-data "D:/Proyecto Final/PyRehab/src/assets;assets/" --paths "D:/Proyecto Final/PyRehab/src" --paths "D:/Proyecto Final/PyRehab/sql" --paths "D:/Proyecto Final/PyRehab/src/Plugins" --hidden-import "yaml" --hidden-import "PluginABC" --hidden-import "PluginABC.PluginABC" --hidden-import "glob2" --hidden-import "math" --hidden-import "random" --add-data "D:/Proyecto Final/PyRehab/src/Plugins;Plugins/"  "D:/Proyecto Final/PyRehab/src/pyrehab.py"