"""view.py 
Funciones para la visualización del menú y de los juegos en PyRehab.
"""
import sys
import pygame
import pygame_menu
import os

sys.path.append(os.path.join(os.getcwd(), "src", "Plugins"))
from utils import *

os.environ["SDL_VIDEO_WINDOW_POS"] = "%d,%d" % (50, 50)


class view:
    """Clase view con funciones para visualizar los juegos"""

    def __init__(self, game_l=[]):
        """
        Inicialización
        : param game_l: Lista de los objetos de los plugins
        """
        self.game_l = game_l
        pygame.init()

    def show_menu(self):
        """Muestra el menú y devuelve la opción seleccionada"""
        info = pygame.display.Info()
        #: Mismo tamaño que pantalla de login
        ancho_menu_view = info.current_w
        alto_menu_view = info.current_h
        surface = pygame.display.set_mode(
            (ancho_menu_view, alto_menu_view)
        )  #: Pantalla de menú

        pygame.display.set_caption("PyRehab")
        icon = pygame.image.load(os.path.join(assets_dir, "logo32.png"))  #: Logo
        pygame.display.set_icon(icon)

        menu = pygame_menu.Menu(
            "PyRehab", ancho_menu_view, alto_menu_view, theme=mytheme
        )  #: Crea el menú

        #: Crea un botón en el menú por cada juego en la lista de plugins. Al accionar el botón, ejecuta la función run de cada juego
        for game in self.game_l:
            menu.add.button(game.name, game.run)

        range_values_discrete = {
            0: "No",
            1: "1",
            2: "2",
            3: "3",
        }  #: Rango para seleccionar volumen
        self.set_volume(volume=2)
        menu.add.range_slider(
            "Volumen",
            2,
            list(range_values_discrete.keys()),
            slider_text_value_enabled=False,
            value_format=lambda x: range_values_discrete[x],
            onchange=self.set_volume,
        )
        menu.add.button(
            "Salir", pygame_menu.events.EXIT
        )  #: Botón para salir del programa

        menu.mainloop(surface)

    def set_volume(self, volume):
        """
        Función para configurar el volumen de los juegos.
        : param volume: Nivel de volumen seleccionado en el menú
        """
        global volumen
        if volume == 1:
            volumen = 0.25
        elif volume == 2:
            volumen = 0.5
        elif volume == 3:
            volumen = 1
        else:
            volumen = 0
        for game in self.game_l:
            game.setup_sound(volumen)  #: configura el volumen en los juegos

    def show_login(self):
        """
        Pantalla de login en PyRehab
        """
        x = 600
        y = 400
        screen = pygame.display.set_mode((x, y))
        screen.fill(lightgreen_unsam)
        #: Fuentes
        pygame.font.init()
        TitFont = pygame.font.Font(fuente, 40)
        InstFont = pygame.font.Font(fuente, 25)
        DNIFont = pygame.font.Font(fuente, 30)
        #: Título
        title = TitFont.render("PyRehab", True, green_unsam)
        screen.blit(title, (10, 20))
        #: Instrucciones
        instr = "Ingrese el DNI y luego presione Entrar"
        blit_text(screen, instr, (10, 100), InstFont, black)
        #: Cuadro de texto
        input_box = pygame.Rect(200, 180, 200, 32)
        pygame.draw.rect(screen, green_unsam, input_box, 2)
        #: Botón de ingreso
        boton = pygame.Rect(0, (y - 50), x, 50)
        pygame.draw.rect(screen, green_unsam, boton)
        screen.blit(InstFont.render("Entrar", True, black), (250, 350))
        # #: Loop
        done = False
        text = ""
        while not done:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    # login = True
                    sys.exit(0)
                elif event.type == pygame.MOUSEBUTTONDOWN:  # si se hace click
                    if boton.collidepoint(event.pos):
                        done = True
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_RETURN:
                        done = True
                    elif event.key == pygame.K_BACKSPACE:
                        text = text[0:-1]
                    else:
                        if event.unicode not in "0123456789":
                            continue
                        if len(text) > 9:
                            continue
                        text += event.unicode

            pygame.draw.rect(screen, green_unsam, input_box, width=0)
            txt_surface = DNIFont.render(text, True, black)
            screen.blit(txt_surface, (input_box.x + 5, input_box.y - 5))
            pygame.display.update()
        return text
