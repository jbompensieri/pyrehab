""" diferencias.py
Juego de las 7 diferencias"""
import sys
import os
import pygame
from pygame.locals import *
import yaml
import time
from datetime import datetime

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
from PluginABC import PluginABC
from utils import *
from model import model

os.environ["SDL_VIDEO_WINDOW_POS"] = "%d,%d" % (50, 50)


class diferencias(PluginABC):
    """Clase diferencias encargada de ejecutar el juego de las 7 diferencias."""

    def __init__(self, name):
        """
        Inicialización
        """
        self.name = name
        pygame.init()

        pygame.mixer.init()
        pygame.font.init()
        self.juego_on = False

    def set_nivel(self, nivel):
        """
        Configuración del nivel
        : param nivel: Nivel del juego
        """
        self.nivel = nivel

    def set_user(self, user):
        """
        Obtiene el DNI del usuario para configurarse luego
        """
        self.dni = user

    def set_up(self):
        """
        Configuraciones iniciales del juego
        """
        # print(f"Setting up plugin {self.name}")

        icon = pygame.image.load(os.path.join(assets_dir, "logo32.png"))
        pygame.display.set_icon(icon)

        self.fuente = os.path.join(
            assets_dir, "SairaUNSAM-MediumSC.ttf"
        )  #: Fuente del texto
        #: Configuraciones según nivel
        if self.nivel == 1:
            self.img_path = os.path.join(diferencias1_dir, "landscape_full.png")
            self.dif_path = os.path.join(diferencias1_dir, "landscape_full.yml")
        elif self.nivel == 2:
            self.img_path = os.path.join(diferencias2_dir, "countryside_full.png")
            self.dif_path = os.path.join(diferencias2_dir, "countryside_full.yml")
        else:
            self.img_path = os.path.join(diferencias3_dir, "park_full.png")
            self.dif_path = os.path.join(diferencias3_dir, "park_full.yml")

        self.img = pygame.image.load(self.img_path)  #: Carga la imagen
        self.img_height = self.img.get_height()  #: Alto de la imagen
        self.img_width = self.img.get_width()  #: Ancho de la imagen
        #: Armado de la grilla para encontrar las diferencias
        grid_offset = 10
        cant_cuadrados = 16
        self.blockWidth = int((self.img_width + grid_offset) / (cant_cuadrados * 2))
        self.blockHeight = int((self.img_height + grid_offset) / cant_cuadrados)

        self.offset = 30  #: Offset de la pantalla que agrega un marco
        self.ancho_pantalla = int(self.img_width)
        self.alto_pantalla = int(self.img_height + self.offset)

        #:  Botón de inicio
        self.ancho_boton = self.ancho_pantalla  #: Ancho del boton
        self.alto_boton = 50  #: Alto del botón
        #: Fuentes
        self.TitleFont = pygame.font.Font(self.fuente, 45)
        self.SubtFont = pygame.font.Font(self.fuente, 35)
        self.InstFont = pygame.font.Font(self.fuente, 30)
        self.TimeFont = pygame.font.Font(self.fuente, 15)

    def setup_sound(self, volume):
        """
        Configuración de los sonidos
        : param volume: Volumen del juego
        """
        # print(f"volume={volume}")
        self.fondo = pygame.mixer.Sound(
            os.path.join(games_sound, "Twin-Musicom-64-Sundays.wav")
        )
        self.fondo.set_volume(volume)
        self.click = pygame.mixer.Sound(os.path.join(games_sound, "click.wav"))
        self.click.set_volume(volume)
        self.correct = pygame.mixer.Sound(os.path.join(games_sound, "correct.wav"))
        self.correct.set_volume(volume)
        self.wrong = pygame.mixer.Sound(os.path.join(games_sound, "wrong.wav"))
        self.wrong.set_volume(volume)
        self.win = pygame.mixer.Sound(os.path.join(games_sound, "win.wav"))
        self.win.set_volume(volume)

    def set_differences(self):
        """
        Función para obtener las diferencias de la imagen a partir de un archivo yml
        : return: Lista de diferencias para esa imagen
        """
        with open(self.dif_path, "r") as f:
            return yaml.load(f)

    def iniciar_juego(self):
        """
        Da inicio al juego
        """
        self.click.play()
        self.fondo.play(-1)  #: Loop infinito para el sonido de fondo
        self.juego_on = True
        self.err_counter = 0
        self.ac_counter = 0

    def show_instructions(self):
        """
        Pantalla de instrucciones del juego
        """
        self.pantalla.fill(white)  #: Necesario para limpiar la pantalla
        menu_title = self.TitleFont.render("Siete Diferencias", True, blue)
        menu_subt = self.SubtFont.render("Instrucciones", True, light_blue)
        menu_instr = (
            "\nObservar bien las dos imágenes, parecen iguales pero no lo son.\n"
            "\n"
            "Identificar las diferencias y hacer click sobre ellas.\n"
            "\n"
            "Para finalizar el juego se deben encontrar las siete diferencias."
        )
        self.pantalla.blit(menu_title, (10, 20))
        self.pantalla.blit(menu_subt, (10, 80))

        blit_text(
            self.pantalla, menu_instr, (10, 100), self.InstFont, black
        )  #: Ajusta el texto al tamaño de la pantalla
        self.boton = pygame.Rect(
            0, (self.alto_pantalla - self.alto_boton), self.ancho_boton, self.alto_boton
        )  #: Botón para iniciar juego
        pygame.draw.rect(self.pantalla, blue, self.boton)
        x = int((self.alto_boton / 2))
        y = int(self.alto_pantalla - self.alto_boton)
        self.pantalla.blit(self.InstFont.render("Iniciar", True, white), (x, y))

    def run(self):
        """
        Ejecuta el juego
        """
        self.set_up()

        self.pantalla = pygame.display.set_mode(
            (self.ancho_pantalla, self.alto_pantalla), pygame.RESIZABLE
        )
        pygame.display.set_caption("PyRehab Siete Diferencias")
        self.pantalla.fill(black)

        done = False  #: Flag para indicar que el juego finalizó
        instr_on = True  #: Flag para indicar que se deben mostrar las instrucciones
        win = False

        #: Variables para marcar el tiempo
        self.segundo = 0
        self.minuto = 0

        MinuteFont = self.TimeFont.render(
            "Tiempo:   {0:02}".format(self.minuto), 1, black
        )
        MinuteFontR = MinuteFont.get_rect()
        MinuteFontR.topright = (self.ancho_pantalla - 60, self.img_height)

        SecondFont = self.TimeFont.render(": {0:02}".format(self.segundo), 1, black)
        SecondFontR = SecondFont.get_rect()
        SecondFontR.topright = (self.ancho_pantalla - 30, self.img_height)

        clock = pygame.time.Clock()
        CLOCKTICK = pygame.USEREVENT + 1
        pygame.time.set_timer(CLOCKTICK, 1000)
        self.clock_rect = pygame.Rect(
            0, self.img_height, self.ancho_pantalla, self.offset
        )  #: Espacio para mostrar el reloj

        for i, x in enumerate(range(0, self.img_width, self.blockWidth)):
            for j, y in enumerate(range(0, self.img_height, self.blockHeight)):
                rect = pygame.Rect(x, y, self.blockWidth, self.blockHeight)
                pygame.draw.rect(self.pantalla, black, rect, 1)  #: Dibuja la grilla
        pygame.display.flip()

        dif_l = self.set_differences()  #: Lista con las diferencias del juego

        #: Loop para mostrar instrucciones
        while instr_on:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    instr_on = False
                    done = True
                    self.finalizar_juego(False)
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    x_abs, y_abs = event.pos
                    #: Si hace click en el botón para iniciar juego
                    if self.boton.collidepoint(event.pos):
                        self.iniciar_juego()  #: Inicia juego
                        instr_on = False  #: Flag que indica que ya no deben mostrarse las instrucciones

            self.show_instructions()  #: Muestra las instrucciones
            pygame.display.update()  #: Actualiza pantalla

        self.pantalla.fill(white)  #: Limpia la pantalla
        self.pantalla.blit(self.img, (0, 0))  #: Muestra imagen
        self.pantalla.blit(SecondFont, SecondFontR)  #: Muestra contador (segundos)
        self.pantalla.blit(MinuteFont, MinuteFontR)  #: Muestra contador (minutos)
        #: Loop del juego
        while not done:
            clock.tick(60)
            for event in pygame.event.get():
                if event.type == CLOCKTICK:
                    if not self.juego_on:
                        continue
                    #: Contador/Timer
                    self.segundo = self.segundo + 1
                    if self.segundo == 60:
                        self.minuto = self.minuto + 1
                        self.segundo = 0
                    #: Actualizo contador
                    SecondFont = self.TimeFont.render(
                        ": {0:02}".format(self.segundo), 1, black
                    )
                    MinuteFont = self.TimeFont.render(
                        "Tiempo:   {0:02}".format(self.minuto), 1, black
                    )
                    #: Muestra el contador en pantalla
                    pygame.draw.rect(self.pantalla, white, self.clock_rect)
                    self.pantalla.blit(SecondFont, SecondFontR)
                    self.pantalla.blit(MinuteFont, MinuteFontR)
                if event.type == pygame.QUIT:
                    self.finalizar_juego(False)
                    self.extraer_datos()
                    done = True
                elif event.type == pygame.KEYDOWN:
                    pass
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    self.click.play()
                    x_abs, y_abs = event.pos
                    cuadro_x = int(
                        x_abs / self.blockWidth
                    )  #: Obtiene cuadro en x correspondiente a la posición del click
                    cuadro_y = int(
                        y_abs / self.blockHeight
                    )  #: Obtiene cuadro en y correspondiente a la posición del click
                    correct = False

                    for i, cuadro in enumerate(dif_l):
                        if (cuadro_x, cuadro_y) in cuadro:
                            pygame.draw.circle(
                                self.pantalla, black, event.pos, 30, width=2
                            )  #: Dibuja círculo alrededor de la posición del click si corresponde a una diferencia
                            pygame.draw.circle(
                                self.pantalla,
                                black,
                                (event.pos[0] - self.img_width / 2, event.pos[1]),
                                30,
                                width=2,
                            )  #: Dibuja círculo alrededor de la posición del click si corresponde a una diferencia (espejado a izquierda)
                            pygame.draw.circle(
                                self.pantalla,
                                black,
                                (event.pos[0] + self.img_width / 2, event.pos[1]),
                                30,
                                width=2,
                            )  #: Dibuja círculo alrededor de la posición del click si corresponde a una diferencia (espejado a derecha)
                            dif_l.remove(dif_l[i])
                            self.correct.play()
                            self.ac_counter += 1
                            correct = True
                            break
                    if not correct:
                        #: Caso de error
                        self.wrong.play()
                        self.err_counter += 1
                    if not dif_l:
                        #: Se fija si encontró todas las diferencias
                        self.win.play()
                        win = True  #: Ganó el juego
                        self.extraer_datos()
                        done = True

                pygame.display.update()
                if win:
                    self.finalizar_juego()  #: Finalizar el juego

    def finalizar_juego(self, wait=True):
        """
        Función para finalizar el juego
        : param wait: Indica si debe esperar al finalizar el juego para cerrar la ventana
        """
        self.fondo.stop()  #: Frena música de fondo
        self.juego_on = False
        if wait:
            time.sleep(2)  #: Espera antes de cerrar la ventana
        self.pantalla = pygame.display.set_mode(
            (ancho_menu_view, alto_menu_view)
        )  #: Vuelve el display al tamaño del menú
        pygame.display.set_caption("PyRehab")

    def extraer_datos(self):
        """
        Extrae datos útiles y los guarda en una tabla en SQL.
        """
        self.model = model()
        fecha = datetime.now()
        fecha = f"{fecha.day}/{fecha.month}/{fecha.year}"
        tiempo = (self.minuto * 60) + self.segundo
        user_data = (
            self.dni,
            self.nivel,
            self.ac_counter,
            self.err_counter,
            tiempo,
            fecha,
        )
        self.model.load_data(
            user_data=user_data, game=self.name
        )  #: Agrega fila en la tabla con los datos del usuario


#: Para correr el juego utilizando el script
# if __name__ == "__main__":
#     diferencias_o = diferencias(name="diferencias")
#     diferencias_o.setup_sound(0.5)
#     diferencias_o.set_nivel(2)
#     diferencias_o.run()
