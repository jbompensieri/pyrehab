import pygame
from pygame.locals import *
import sys
import os
from datetime import datetime
import time
import math
import random
import glob2

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
from PluginABC import PluginABC
from utils import *
from model import model

#: Indica dónde abrir el display en pantalla
os.environ["SDL_VIDEO_WINDOW_POS"] = "%d,%d" % (50, 50)


class memotest(PluginABC):
    """Clase memotest encargada de ejecutar el juego memotest."""

    def __init__(self, name):
        """
        Inicialización
        """
        self.name = name
        # print(f"Init {self.name}")
        pygame.init()
        pygame.mixer.init()
        pygame.font.init()
        #: Flags
        self.gano = False
        self.juego_on = False  #: Para saber si el juego está iniciado

    def set_nivel(self, nivel):
        """
        Configuración del nivel
        : param nivel: Nivel del juego
        """
        self.nivel = nivel

    def set_user(self, user):
        """
        Obtención del usuario
        :param user: DNI del jugador
        """
        self.dni = user

    def set_up(self):
        """
        Set Up del juego - configuraciones iniciales
        """
        # print(f"Setting up plugin {self.name}")

        #: Logo e icono
        self.logo = pygame.image.load(os.path.join(assets_dir, "logo.png"))
        icon = pygame.image.load(os.path.join(assets_dir, "logo32.png"))
        pygame.display.set_icon(icon)

        #: Configuracion de la pantalla segun cantidad de imagenes
        #: Cuadro es una funcion para crear la grilla de imágenes en pantalla
        self.cuadro_l = self.cuadro()

        self.alto_boton = 50  #: Altura del botón de inicio
        self.px_cuadro = 150  #: Pixeles de los cuadros. Tienen que coincidir con el tamaño de las imágenes

        #: Cálculo automático de filas y columnas según cantidad de imágenes
        self.filas = round(math.sqrt(self.max_img * 2))
        self.columnas = (
            self.filas if self.filas**2 == self.max_img else self.filas + 1
        )
        if ((self.filas * self.columnas) - self.columnas) >= (self.max_img * 2):
            self.filas -= 1

        #: Tamaño de la pantalla
        self.offset = 30  #: Para separar las imágenes del borde de la pantalla
        self.ancho_pantalla = int(self.px_cuadro * self.columnas + self.offset)
        self.alto_pantalla = int(self.px_cuadro * self.filas + self.offset)

        #: Botón de inicio
        self.ancho_boton = self.ancho_pantalla  # Ancho del boton ocupa toda la pantalla

        #: Fuentes
        self.fuente = os.path.join(assets_dir, "SairaUNSAM-MediumSC.ttf")
        self.TitleFont = pygame.font.Font(self.fuente, 45)
        self.SubtFont = pygame.font.Font(self.fuente, 35)
        self.InstFont = pygame.font.Font(self.fuente, 25)
        self.TimeFont = pygame.font.Font(self.fuente, 15)

        #: Segundos que muestra las cartas volteadas en caso de ser incorrecto
        self.sec_mostrar = 2

    def setup_sound(self, volume):
        """
        Set Up de sonidos
        : param volume: Volumen
        """
        # print(f"volume={volume}")
        self.fondo = pygame.mixer.Sound(
            os.path.join(games_sound, "Twin-Musicom-64-Sundays.wav")
        )
        self.fondo.set_volume(volume)
        self.click = pygame.mixer.Sound(os.path.join(games_sound, "click.wav"))
        self.click.set_volume(volume)
        self.flip = pygame.mixer.Sound(os.path.join(games_sound, "flip.wav"))
        self.flip.set_volume(volume)
        self.correct = pygame.mixer.Sound(os.path.join(games_sound, "correct.wav"))
        self.correct.set_volume(volume)
        self.wrong = pygame.mixer.Sound(os.path.join(games_sound, "wrong.wav"))
        self.wrong.set_volume(volume)
        self.win = pygame.mixer.Sound(os.path.join(games_sound, "win.wav"))
        self.win.set_volume(volume)

    def extraer_datos(self):
        """Extrae datos útiles y los guarda en una tabla de SQL."""
        self.model = model()
        fecha = datetime.now()
        fecha = f"{fecha.day}/{fecha.month}/{fecha.year}"
        tiempo = (self.minuto * 60) + self.segundo
        user_data = (
            self.dni,
            self.nivel,
            self.ac_counter,
            self.err_counter,
            tiempo,
            fecha,
        )
        self.model.load_data(user_data=user_data, game=self.name)

    def cuadro(self):
        """
        Función para armar la pantalla con cuadros e íconos
        """
        cuadro_l = []
        #: Direcciones hacia las carpetas con íconos, según nivel
        path1 = os.path.join(memotest1_dir, "*.png")
        path2 = os.path.join(memotest2_dir, "*.png")
        path3 = os.path.join(memotest3_dir, "*.png")
        path4 = os.path.join(memotest4_dir, "*.png")
        #: Configuraciones según nivel
        if self.nivel == 1:
            self.path = path1
            self.max_img = 6
        elif self.nivel == 2:
            self.path = path2
            self.max_img = 10
        elif self.nivel == 3:
            self.path = path3
            self.max_img = 12
        else:
            self.path = path4
            self.max_img = 12

        path_l = [path_img for path_img in glob2.glob(self.path)]
        random.shuffle(path_l)  #:Elige imágenes al azar

        for i, path_img in enumerate(path_l):
            if i >= self.max_img:
                break
            for _ in [1, 2]:  # Guardo dos imágenes iguales para la dupla del juego
                imagen = pygame.image.load(path_img)
                d = {
                    "mostrar": False,
                    "descubierto": False,
                    "path_img": path_img,
                    "img": imagen,
                    "rtg": imagen.get_rect(),
                }  #: Diccionario con los keys que tiene cada cuadro
                cuadro_l.append(d)
        return cuadro_l

    def ocultar_cuadros(self):
        """
        Función para ocultar los cuadros. Se usa al inicio y cuando no hubo coincidencia en el juego
        """
        for d in self.cuadro_l:
            d["mostrar"] = False
            d["descubierto"] = False

    def ganador(self):
        """
        Función para conocer si el jugador ganó el juego
        """
        for d in self.cuadro_l:
            if not d["descubierto"]:
                return False  #: Retorna False si al menos un cuadro no está descubierto
        return True  #: Si todas las imagenes están descubiertas, retorna True lo que significa que ganó

    def finalizar_juego(self, wait=True):
        """
        Función para indicar que finalizó el juego
        : param wait: Indica si se debe o no esperar un delay para cerrar la ventana una vez finalizado el juego
        """
        self.fondo.stop()  #: Para la música de fondo
        self.juego_on = False  #: Flag para indicar que el juego finalizó
        if wait:
            time.sleep(2)  #: Delay para cerrar ventana
        #: Cierra ventana y vuelve al tamaño del menú
        self.pantalla = pygame.display.set_mode((ancho_menu_view, alto_menu_view))
        pygame.display.set_caption("PyRehab")

    def iniciar_juego(self):
        """
        Función para indicar que inicia el juego
        """
        random.shuffle(self.cuadro_l)  #: Randomizo el orden de las imagenes
        self.err_counter = 0  #: Contador de errores
        self.ac_counter = 0  #: Contador de aciertos
        self.click.play()
        self.fondo.play(-1)  #: Sonido de fondo en loop infinito
        self.ocultar_cuadros()  #: Los cuadros inician ocultos
        self.juego_on = True  #: Flag para indicar que el juego está iniciado

    def show_instructions(self):
        """
        Función para mostrar pantalla con instrucciones
        """
        self.pantalla.fill(white)
        menu_title = self.TitleFont.render("Memotest", True, blue)
        menu_subt = self.SubtFont.render("Instrucciones", True, light_blue)
        menu_instr = (
            "\nHacer click sobre los íconos y encontrar las imágenes iguales.\n"
            "Si las imágenes no son iguales, se vuelven a ocultar.\n"
            "\n"
            "Para finalizar el juego se deben encontrar todos los pares de imágenes."
        )
        self.pantalla.blit(menu_title, (10, 20))
        self.pantalla.blit(menu_subt, (10, 80))

        blit_text(
            self.pantalla, menu_instr, (10, 100), self.InstFont, black
        )  #: Función para mostrar instrucciones dependiendo del tamaño de la pantalla
        self.boton = pygame.Rect(
            0, (self.alto_pantalla - self.alto_boton), self.ancho_boton, self.alto_boton
        )
        pygame.draw.rect(self.pantalla, blue, self.boton)
        x = int((self.alto_boton / 2))
        y = int(self.alto_pantalla - self.alto_boton)
        self.pantalla.blit(self.InstFont.render("Iniciar", True, white), (x, y))

    def run(self):
        """
        Función que ejecuta el juego
        """
        # Inicia la pantalla y el sonido de fondo
        self.set_up()

        self.pantalla = pygame.display.set_mode(
            (self.ancho_pantalla, self.alto_pantalla), pygame.RESIZABLE
        )
        pygame.display.set_caption("PyRehab Memotest")
        volteadas = []  #: Lista vacía para guardar las imágenes volteadas

        #: Flags que controlan los loops
        done = False
        instr_on = True

        #: Time Info
        self.segundo = 0
        self.minuto = 0

        #: Contador minutos
        MinuteFont = self.TimeFont.render(
            "Tiempo:   {0:02}".format(self.minuto), 1, black
        )
        MinuteFontR = MinuteFont.get_rect()
        MinuteFontR.topright = (self.ancho_pantalla - 60, 0)
        #: Contador segundos
        SecondFont = self.TimeFont.render(": {0:02}".format(self.segundo), 1, black)
        SecondFontR = SecondFont.get_rect()
        SecondFontR.topright = (self.ancho_pantalla - 30, 0)

        Clock = pygame.time.Clock()
        CLOCKTICK = pygame.USEREVENT + 1
        pygame.time.set_timer(CLOCKTICK, 1000)

        #: Loop del juego
        while not done:
            #: Loop para mostrar instrucciones
            while instr_on:
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        #: En caso de cerrar el programa
                        instr_on = False
                        done = True
                        self.finalizar_juego(False)
                    elif event.type == pygame.MOUSEBUTTONDOWN:
                        #: Evento para detectar clic sobre la pantalla
                        x_abs, y_abs = event.pos  #: Posiciones del click
                        #: Si hace clic en el botón para iniciar juego
                        if self.boton.collidepoint(event.pos):
                            self.iniciar_juego()  #: Inicia juego
                            instr_on = False  #: Sale del loop de instrucciones

                #: Mientras no haya eventos
                self.show_instructions()  #: Muestra las instrucciones
                pygame.display.update()

            #: Fuera de las instrucciones
            self.pantalla.fill(white)  #: Limpio la pantalla
            esperar = False

            #: Vuelvo a detectar eventos (en loop del juego)
            for event in pygame.event.get():
                if event.type == CLOCKTICK:  # Contador del clock
                    if not self.juego_on:
                        continue
                    #: Timer
                    self.segundo = self.segundo + 1
                    if self.segundo == 60:
                        self.minuto = self.minuto + 1
                        self.segundo = 0
                    #: Vuelve a dibujar el tiempo en cada ciclo de clock
                    SecondFont = self.TimeFont.render(
                        ": {0:02}".format(self.segundo), 1, black
                    )
                    MinuteFont = self.TimeFont.render(
                        "Tiempo:   {0:02}".format(self.minuto), 1, black
                    )

                if event.type == pygame.QUIT:
                    #: Si hace clic para cerrar el juego
                    self.finalizar_juego(False)  #: Finaliza el juego sin esperar
                    self.extraer_datos()  #: Extrae los datos obtenidos hasta el momento
                    done = True
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    #: Si se hace click y se puede jugar
                    x_abs, y_abs = event.pos  #: Posiciones del click
                    # print(x_abs, y_abs)
                    sel = None
                    for i, d in enumerate(self.cuadro_l):
                        if d["rtg"].collidepoint(event.pos):
                            #: Si hace clic sobre un rectángulo/cuadro/ícono
                            sel = i  #: Guarda selección
                            break
                    if sel is None:
                        continue

                    if (
                        self.cuadro_l[sel]["mostrar"]
                        or self.cuadro_l[sel]["descubierto"]
                    ):
                        continue  #: Ignora el clic porque ese cuadro ya fue tocado

                    volteadas.append(sel)  #: Guardo el índice de la imagen clickeada

                    if len(volteadas) == 1:
                        self.cuadro_l[sel][
                            "mostrar"
                        ] = True  #: Marca el cuadro como mostrado
                        self.flip.play()
                    else:
                        #: Si hay dos imágenes volteadas
                        self.cuadro_l[sel]["mostrar"] = True
                        sel1, sel2 = volteadas
                        if (
                            self.cuadro_l[sel1]["path_img"]
                            == self.cuadro_l[sel2]["path_img"]
                        ):  #: Chequea si los cuadros tocados son iguales
                            #: Ambos cuadros se guardan como descubiertos (quedan volteados en pantalla porque se encontró coincidencia)
                            self.cuadro_l[sel1]["descubierto"] = True
                            self.cuadro_l[sel2]["descubierto"] = True
                            self.correct.play()  #: Sonido de acierto
                            self.ac_counter += 1  #: Suma un acierto
                        else:
                            #: Si no coinciden
                            self.wrong.play()  #: Sonido de error
                            self.err_counter += 1  #: Suma un error
                            esperar = True  #: Muestra unos segundos las imágenes antes de ocultarlas
            Clock.tick(60)
            r = self.offset / 2
            counter = 0
            #: Algoritmo para armar la pantalla con los cuadros
            for i in range(self.filas):
                q = self.offset / 2
                for j in range(self.columnas):
                    if counter > (self.max_img * 2) - 1:
                        break
                    d = self.cuadro_l[counter]
                    rtg = d["rtg"]
                    rtg.topleft = (q, r)  #: Posicion del rectangulo
                    if (
                        self.cuadro_l[counter]["mostrar"]
                        or self.cuadro_l[counter]["descubierto"]
                    ):
                        self.pantalla.blit(
                            self.cuadro_l[counter]["img"], rtg
                        )  #: Muestra la imagen
                    else:
                        self.pantalla.blit(
                            self.logo, rtg
                        )  #: Muestra el logo (imagen oculta)
                    #: Muevo coordenadas para dibujar en pantalla el siguiente cuadro
                    q += self.px_cuadro
                    counter += 1
                r += self.px_cuadro + 1
            #: Muestra contador en pantalla
            self.pantalla.blit(SecondFont, SecondFontR)
            self.pantalla.blit(MinuteFont, MinuteFontR)

            pygame.display.update()

            if len(volteadas) >= 2:
                #: Cuando se encuentra 1 coincidencia o más
                if esperar:
                    time.sleep(
                        1
                    )  #: Espero antes de volver a dar vuelta las imagenes que no matchearon
                    self.cuadro_l[volteadas[0]]["mostrar"] = False
                    self.cuadro_l[volteadas[1]]["mostrar"] = False
                if self.ganador():
                    #: Comprueba si gana
                    self.win.play()  #: Sonido de ganador
                    self.finalizar_juego()  #: Finaliza el juego
                    self.extraer_datos()  #: Extrae parámetros del juego
                    done = True  #: Finaliza loop
                volteadas = []


#: Para correr el juego usando el script
# if __name__ == "__main__":
#     memotest_o = memotest(name="memotest")
#     memotest_o.set_nivel(1)
#     memotest_o.setup_sound(0.5)
#     memotest_o.run()
