from nturl2path import pathname2url
import sys
import os
import pygame
from pygame.locals import *
import time
import random
import glob2
from datetime import datetime

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
from PluginABC import PluginABC
from utils import *
from model import model

#: Indica dónde abrir el display en pantalla
os.environ["SDL_VIDEO_WINDOW_POS"] = "%d,%d" % (50, 50)


class escalas(PluginABC):
    """Clase escalas encargada de ejecutar el juego escalas."""

    def __init__(self, name):
        """
        Inicio Pygame
        : param name: Nombre del juego
        """
        self.name = name
        # print(f"Init {self.name}")
        pygame.init()
        pygame.mixer.init()
        pygame.font.init()
        #: Flags
        self.gano = False
        self.juego_on = False  #: Para saber si el juego está iniciado

    def set_nivel(self, nivel):
        """
        Configuración del nivel
        : param nivel: Nivel del juego
        """
        self.nivel = nivel

    def set_user(self, user):
        """
        Obtención del usuario
        : param user: DNI del jugador
        """
        self.dni = user

    def set_up(self):
        """
        Set Up del juego - configuraciones iniciales
        """
        # print(f"Setting up plugin {self.name}")

        #: Icono
        icon = pygame.image.load(os.path.join(assets_dir, "logo32.png"))
        pygame.display.set_icon(icon)

        self.alto_boton = 50  #: Altura del botón de inicio
        self.px_cuadro = 150  #: Pixeles de los cuadros. Maximo tamaño de las imágenes

        #: Configuraciones según nivel
        if self.nivel == 1:
            self.filas = 3
            self.columnas = 4
        elif self.nivel == 2:
            self.filas = 4
            self.columnas = 4
        else:
            self.filas = 4
            self.columnas = 5

        self.cuadro_l = self.cuadro()  #: Lista con los cuadros con íconos
        #: Configuracion de la pantalla
        self.offset = 30  #: Para separar las imágenes del borde de la pantalla
        self.ancho_pantalla = int(self.px_cuadro * self.columnas + self.offset)
        self.alto_pantalla = int(self.px_cuadro * self.filas + self.offset + 20)
        #: Botón de inicio
        self.ancho_boton = self.ancho_pantalla

        #: Fuentes
        self.fuente = os.path.join(assets_dir, "SairaUNSAM-MediumSC.ttf")
        self.TitleFont = pygame.font.Font(self.fuente, 45)
        self.SubtFont = pygame.font.Font(self.fuente, 35)
        self.InstFont = pygame.font.Font(self.fuente, 25)
        self.TimeFont = pygame.font.Font(self.fuente, 15)

    def setup_sound(self, volume):
        """
        Configuración de los sonidos
        : param volume: Volumen
        """
        # print(f"volume={volume}")
        self.fondo = pygame.mixer.Sound(
            os.path.join(games_sound, "Twin-Musicom-64-Sundays.wav")
        )
        self.fondo.set_volume(volume)
        self.click = pygame.mixer.Sound(os.path.join(games_sound, "click.wav"))
        self.click.set_volume(volume)
        self.correct = pygame.mixer.Sound(os.path.join(games_sound, "correct.wav"))
        self.correct.set_volume(volume)
        self.wrong = pygame.mixer.Sound(os.path.join(games_sound, "wrong.wav"))
        self.wrong.set_volume(volume)
        self.win = pygame.mixer.Sound(os.path.join(games_sound, "win.wav"))
        self.win.set_volume(volume)

    def extraer_datos(self):
        """
        Extrae datos útiles y los guarda en una tabla en SQL.
        """
        self.model = model()
        fecha = datetime.now()
        fecha = f"{fecha.day}/{fecha.month}/{fecha.year}"
        tiempo = (self.minuto * 60) + self.segundo
        user_data = (
            self.dni,
            self.nivel,
            self.ac_counter,
            self.err_counter,
            tiempo,
            fecha,
        )
        self.model.load_data(user_data=user_data, game=self.name)

    def cuadro(self):
        """
        Genera los cuadros a mostrar en la pantalla
        : return: Lista con los cuadros a mostrar en pantalla
        """
        cuadro_l = []
        self.path = os.path.join(
            escalas_dir, "*.png"
        )  #: Dirección hacia la carpeta con imágenes
        #: Configuraciones según nivel
        if self.nivel == 1:
            #: Nivel fácil
            range_inf = 30
            range_sup = 125
            lim = 0.17
        elif self.nivel == 2:
            #: Nivel medio
            range_inf = 40
            range_sup = 125
            lim = 0.15
        else:
            #: Nivel difícil
            range_inf = 70
            range_sup = 125
            lim = 0.10

        path_l = [path_img for path_img in glob2.glob(self.path)]
        random.shuffle(path_l)  #: Elije imágenes random

        for i, path_img in enumerate(path_l):
            if i >= self.filas:
                break
            for j in range(self.columnas):
                scale_correct = False  #: Flag para saber si la escala está correcta (respeta self.lim)
                imagen = pygame.image.load(path_img)  #: Imágen original
                red_ini = 0.8  #: Reduce la imagen a un porcentaje del tamaño inicial (0.8 = 80%)
                scale_init = tuple(
                    [int(i * red_ini) for i in imagen.get_rect().size]
                )  #: Escala inicial
                imagen_scaled = pygame.transform.scale(
                    imagen, scale_init
                )  #: Imagen inicial escalada

                if j < 2:
                    #: Genero dos imágenes con la misma escala (imagen inicial)
                    scale = 1
                else:
                    #: Para el resto de las imágenes (columnas)
                    while not scale_correct:
                        scale = (
                            random.randrange(range_inf, range_sup) / 100
                        )  #: Genero una escala random entre dos límites, dependiendo el nivel
                        scale_correct = self.check_scale(
                            scale, cuadro_l[-j:], lim
                        )  #: Verifico que sea una escala correcta, fila por fila
                    #: Si la escala es correcta
                    new_size = tuple(
                        [int(i * red_ini * scale) for i in imagen.get_rect().size]
                    )  #: Calculo la nueva escala
                    imagen_scaled = pygame.transform.scale(
                        imagen, new_size
                    )  #: Modifico la imagen según la escala
                    # print('j=', j, 'new_size=', new_size, 'scale=', scale)

                #: Genero un diccionario con las imágenes y sus características
                d = {
                    "img": imagen_scaled,
                    "rtg": imagen_scaled.get_rect(),
                    "scale": scale,
                    "tocada": False,
                    "fila": i,
                }
                cuadro_l.append(d)  #: Lista de diccionarios (uno por cada imagen)
            #: Cambio de lugar las imágenes para que no esten ordenadas. La primera columna mantiene las imágenes originales
            cuadro_copy = cuadro_l[-self.columnas + 1 :]
            random.shuffle(cuadro_copy)
            cuadro_l[-self.columnas + 1 :] = cuadro_copy
        return cuadro_l

    def check_scale(self, scale, cuadro_l, lim):
        """
        Verifico la escala de la imagen, según un límite.
        : param scale: Escala
        : param cuadro_l: Lista de cuadros/imágenes
        : param lim: Límite entre escalas para generar imágenes más o menos parecidas
        """
        limite = lim  #: Diferencia máxima entre escalas
        for d in cuadro_l:
            if abs(scale - d["scale"]) < limite:
                #: Si la diferencia entre la escala calculada y las de las otras imágenes de la fila es menor que el límite
                return False
        return True

    def ganador(self):
        """
        Verifica si ganó el juego
        """
        for d in self.cuadro_l:
            if not d["tocada"]:
                return False  #: Retorna False si falta descubrir una imágen
        return True  #: Si todas las imagenes están descubiertas, retorna True lo que significa que ganó

    def finalizar_juego(self, wait=True):
        """
        Función que indica que el juego finalizó
        : param wait: Indica si se debe esperar al finalizar
        """
        self.fondo.stop()  #: Finaliza sonido de fondo
        self.juego_on = False  #: Indica que el juego finalizó
        if wait:
            time.sleep(2)  #: Espera antes de cerrar la pantalla
        self.pantalla = pygame.display.set_mode(
            (ancho_menu_view, alto_menu_view)
        )  #: Vuelve el display al tamaño del menú
        pygame.display.set_caption("PyRehab")

    def iniciar_juego(self):
        """
        Función para indicar que inicia el juego
        """
        self.err_counter = 0  #: Contador de errores
        self.ac_counter = 0  #: Contador de aciertos
        self.click.play()
        self.fondo.play(-1)  #: Música de fondo en loop infinito
        self.juego_on = True  #: Indica juego iniciado

    def show_instructions(self):
        """
        Función para mostrar pantalla de instrucciones
        """
        self.pantalla.fill(white)
        menu_title = self.TitleFont.render("Escalas", True, blue)
        menu_subt = self.SubtFont.render("Instrucciones", True, light_blue)
        menu_instr = (
            "\nHacer click sobre la imagen que tiene el mismo tamaño que la imagen original a la izquierda de la pantalla.\n\n"
            "Para finalizar el juego se deben seleccionar todas las imágenes del mismo tamaño."
        )
        self.pantalla.blit(menu_title, (10, 20))
        self.pantalla.blit(menu_subt, (10, 80))

        blit_text(
            self.pantalla, menu_instr, (10, 100), self.InstFont, black
        )  #: Función para mostrar instrucciones en pantalla según el tamaño del display
        self.boton = pygame.Rect(
            0, (self.alto_pantalla - self.alto_boton), self.ancho_boton, self.alto_boton
        )
        pygame.draw.rect(self.pantalla, blue, self.boton)
        x = int((self.alto_boton / 2))
        y = int(self.alto_pantalla - self.alto_boton)
        self.pantalla.blit(self.InstFont.render("Iniciar", True, white), (x, y))

    def run(self):
        """
        Función que ejecuta el juego
        """
        #: Inicia la pantalla y el sonido de fondo
        self.set_up()

        self.pantalla = pygame.display.set_mode(
            (self.ancho_pantalla, self.alto_pantalla), pygame.RESIZABLE
        )
        pygame.display.set_caption("PyRehab Escalas")

        #: Flags que controlan los loops
        done = False
        instr_on = True

        #: Time Info
        self.segundo = 0
        self.minuto = 0

        MinuteFont = self.TimeFont.render(
            "Tiempo:   {0:02}".format(self.minuto), 1, black
        )
        MinuteFontR = MinuteFont.get_rect()
        MinuteFontR.topright = (self.ancho_pantalla - 60, 0)

        SecondFont = self.TimeFont.render(": {0:02}".format(self.segundo), 1, black)
        SecondFontR = SecondFont.get_rect()
        SecondFontR.topright = (self.ancho_pantalla - 30, 0)

        clock = pygame.time.Clock()
        CLOCKTICK = pygame.USEREVENT + 1
        pygame.time.set_timer(CLOCKTICK, 1000)

        while not done:
            while instr_on:
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        #: Si cierra el programa
                        instr_on = False
                        done = True
                        self.finalizar_juego(False)  #: Finaliza el juego
                    elif event.type == pygame.MOUSEBUTTONDOWN:
                        #: Si hace clic en la pantalla
                        x_abs, y_abs = event.pos  #: Posiciones del click
                        if self.boton.collidepoint(
                            event.pos
                        ):  #: Si hace click en el botón para iniciar juego
                            self.iniciar_juego()  #: inicia juego
                            instr_on = False  #: sale del loop de las instrucciones
                #: Mientras no haya eventos
                self.show_instructions()  #: muestra las instrucciones
                pygame.display.update()

            #: Fuera de las instrucciones
            self.pantalla.fill(white)  #: Limpio la pantalla
            #: Vuelvo a detectar eventos (en el loop del juego)
            clock.tick(60)
            for event in pygame.event.get():
                if event.type == CLOCKTICK:
                    if not self.juego_on:
                        continue
                    #: Timer
                    self.segundo = self.segundo + 1
                    if self.segundo == 60:
                        self.minuto = self.minuto + 1
                        self.segundo = 0
                    #: Vuelve a dibujar tiempo en cada ciclo de clock
                    SecondFont = self.TimeFont.render(
                        ": {0:02}".format(self.segundo), 1, black
                    )
                    MinuteFont = self.TimeFont.render(
                        "Tiempo:   {0:02}".format(self.minuto), 1, black
                    )

                if event.type == pygame.QUIT:
                    #: Si cierra el programa
                    done = True
                    self.finalizar_juego(False)  #: Finaliza el juego
                    self.extraer_datos()  #: Extrae datos de los parámetros del juego hasta el momento
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    #: si se hace click y se puede jugar
                    x_abs, y_abs = event.pos  #: posiciones del click
                    sel = None  #: variable para guardar el click
                    for i, d in enumerate(self.cuadro_l):
                        if d["rtg"].collidepoint(event.pos) and x_abs > (
                            self.px_cuadro + self.offset
                        ):
                            #: verifico si hago click sobre un cuadro fuera de la primera columna
                            sel = i  #: guardo el índice de cuadro clickeado
                            break
                        else:  #: verifico si estoy en la primera columna
                            continue  #: ignoro el click
                    if sel is None:  #: en caso qe no haya click
                        continue

                    if not (self.cuadro_l[sel]["tocada"]):
                        #: verifica si la imagen no fue tocada (o fila)
                        if self.cuadro_l[0]["scale"] == self.cuadro_l[sel]["scale"]:
                            #: verifica que tengan las mismas escalas
                            pygame.draw.rect(
                                self.cuadro_l[sel]["img"], green, [0, 0, 120, 120], 3
                            )  #: dibuja un cuadrado en la correcta
                            self.correct.play()  #: sonido de acierto
                            self.ac_counter += 1  #: Suma un acierto

                            fila = self.cuadro_l[sel]["fila"]
                            for index, im in enumerate(self.cuadro_l):
                                if im["fila"] == fila:
                                    self.cuadro_l[index][
                                        "tocada"
                                    ] = True  #: marco toda la fila como tocada
                        else:  #: si no coinciden
                            self.wrong.play()  #: Sonido de error
                            self.err_counter += 1  #: Suma un error
            r = self.alto_boton * 2.5
            counter = 0
            #: Algoritmo para mostrar imágenes en pantalla
            for i in range(self.filas):
                q = self.px_cuadro / 2
                for j in range(self.columnas):
                    if counter > (self.columnas * self.filas) - 1:
                        break
                    d = self.cuadro_l[counter]
                    rtg = d["rtg"]
                    rtg.center = (q, r)  #: Posicion del rectangulo
                    self.pantalla.blit(
                        self.cuadro_l[counter]["img"], rtg
                    )  #: Muestra la imágen en pantalla
                    #: Mueve los indicadores de posición
                    q += self.px_cuadro
                    counter += 1
                r += self.px_cuadro

            #: Dibuja tiempo en pantalla
            self.pantalla.blit(SecondFont, SecondFontR)
            self.pantalla.blit(MinuteFont, MinuteFontR)
            #: Dibuja grilla en pantalla
            for y in range(
                self.alto_boton, self.alto_pantalla - self.alto_boton, self.px_cuadro
            ):
                for x in range(0, self.px_cuadro, self.px_cuadro):
                    rect = pygame.Rect(x, y, self.px_cuadro, self.px_cuadro)
                    pygame.draw.rect(self.pantalla, black, rect, 3)

            pygame.display.update()
            if self.ganador():
                self.win.play()  #: Sonido de ganador
                self.finalizar_juego()  #: Finaliza el juego
                self.extraer_datos()  #: Extrae datos
                done = True  #: Finaliza loop del juego


#: PAra correr el juego usando el script
# if __name__ == "__main__":
#     escalas_o = escalas(name="escalas")
#     escalas_o.setup_sound(0.5)
#     escalas_o.set_nivel(1)
#     escalas_o.run()
