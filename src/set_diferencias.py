"""set_diferencias.py 
Permite generar el listado de cuadros donde se encuentran las diferencias del juego de 7 diferencias.
A partir de la imagen y de una grilla, cuando el programador selecciona la diferencia, se almacena
en una lista que luego se utilizará en el juego para determinar la ubicación de las diferencias."""
import sys
import os
import pygame
from pygame.locals import *
import yaml

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
# from PluginABC import PluginABC
from utils import *


class set_diferencias:
    """Clase set_diferencias con las funciones que permiten abrir la imagen y seleccionar las diferencias."""

    def __init__(self, name):
        """
        Inicialización
        """
        self.name = name
        pygame.init()
        self.logo = pygame.image.load(
            os.path.join(assets_dir, "logo.png")
        )  #: Logo de PyRehab
        self.clock = pygame.time.Clock()

    def set_up(self):
        """
        Configuración de la pantalla. Permite abrir la imagen en pantalla.
        """

        file_name = "landscape_full.png"  #: Nombre de la imagen a abrir
        self.path = os.path.join(diferencias1_dir, file_name)  #: Path de la imagen
        self.img = pygame.image.load(self.path)  #: Carga imagen
        self.img_height = self.img.get_height()  #: Ancho de la imagen
        self.img_width = self.img.get_width()  #: Alto de la imagen

        self.pantalla = pygame.display.set_mode(
            (self.img_width, self.img_height)
        )  #: Pantalla adaptada al tamaño de la imagen
        self.pantalla.fill(white)
        offset = 10
        self.cant_cuadrados = 16
        self.blockWidth = int(
            (self.img_width + offset) / (self.cant_cuadrados * 2)
        )  # Ancho de los cuadrados de la grilla
        self.blockHeight = int(
            (self.img_height + offset) / self.cant_cuadrados
        )  #: Altura de los cuadrados de la grilla

    def get_cuadros(self, pos):
        """
        Función para obtener los cuadros donde se seleccionan las diferencias
        : param pos: Coordenadas del click
        : return: Coordenadas del cuadro donde están las diferencias
        """
        x_abs, y_abs = pos  #: Posiciones del click
        return int(x_abs / self.blockWidth), int(y_abs / self.blockHeight)

    def get_diff_list(self):
        """
        Función para obtener la lista de coordenadas de cuadros con diferencias. Se guarda en un archivo .yml
        """
        done = False
        diff_all_list = []  #: Lista con todas las diferencias
        diff_set_list = []  #: Lista con todas las coordenadas para la misma diferencia
        self.pantalla.blit(self.img, (0, 0))  #: Muestra imagen en pantalla
        for i, x in enumerate(range(0, self.img_width, self.blockWidth)):
            for j, y in enumerate(range(0, self.img_height, self.blockHeight)):
                rect = pygame.Rect(x, y, self.blockWidth, self.blockHeight)
                pygame.draw.rect(
                    self.pantalla, black, rect, 1
                )  #: Dibuja grilla en pantalla
        while not done:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    done = True  #: Cierra el programa
                elif event.type == pygame.KEYDOWN:
                    if diff_set_list:
                        print("Append diff_set_list")
                        diff_all_list.append(
                            diff_set_list
                        )  #: Agrega las coordenadas a la lista de todas las diferencias cuando presiona enter
                        print(diff_all_list)
                        diff_set_list = (
                            []
                        )  #: Limpia lista de coordenadas de la misma diferencia
                    else:
                        done = True
                        break
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    cuadro_x, cuadro_y = self.get_cuadros(
                        event.pos
                    )  #: Obtiene las coordenadas de los cuadros correspondientes a la posición del mouse
                    print("New difference:")
                    print(f"\t{cuadro_x}\t{cuadro_y}")
                    diff_set_list.append(
                        (cuadro_x, cuadro_y)
                    )  #: Agrega el cuadro tocado
                    diff_set_list.append(
                        (cuadro_x + self.cant_cuadrados, cuadro_y)
                    )  #: Agrega el cuadro espejado a la derecha
                    diff_set_list.append(
                        (cuadro_x - self.cant_cuadrados, cuadro_y)
                    )  #: Agrega el cuadro espejado a la izquierda
                    pygame.draw.circle(
                        self.pantalla, black, event.pos, 30, width=2
                    )  #: Dibuja un circulo en la diferencia seleccionada
                    pygame.draw.circle(
                        self.pantalla,
                        black,
                        (event.pos[0] + self.img_width / 2, event.pos[1]),
                        30,
                        width=2,
                    )  #: Dibuja un circulo en la diferencia espejada a la derecha
                    pygame.draw.circle(
                        self.pantalla,
                        black,
                        (event.pos[0] - self.img_width / 2, event.pos[1]),
                        30,
                        width=2,
                    )  #: Dibuja un circulo en la diferencia espejada a la izquierda

                pygame.display.update()
        yaml_path = os.path.join(self.path, os.path.split(self.path)[-1])
        yaml_path = yaml_path.split(".")[0]
        yaml_path = (
            f"{yaml_path}.yml"  #: Path al archivo yml con el nombre de la imagen
        )
        # print(yaml_path)
        with open(yaml_path, "w") as f:
            f.write(yaml.dump(diff_all_list))  #: Escribe el archivo
            done = True


set_diferencias_o = set_diferencias(name="set_diferencias")
set_diferencias_o.set_up()
set_diferencias_o.get_diff_list()
