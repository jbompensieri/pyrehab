import abc


class PluginABC(metaclass=abc.ABCMeta):
    """Plugin Abstract Base clase con los métodos obligatorios."""

    @abc.abstractmethod
    def set_nivel(self, nivel):
        """
        Configuración del nivel
        : param nivel: Nivel del juego
        """
        pass

    @abc.abstractmethod
    def set_user(self, user):
        """
        Obtiene el DNI del usuario para configurarse luego
        """
        pass

    @abc.abstractmethod
    def set_up(self):
        """Set up method."""
        pass

    @abc.abstractmethod
    def setup_sound(self, volume):
        """
        Configuración de los sonidos
        : param volume: Volumen del juego
        """
        pass

    @abc.abstractmethod
    def show_instructions(self):
        """
        Pantalla de instrucciones del juego
        """
        pass

    @abc.abstractmethod
    def extraer_datos(self):
        """
        Extrae datos útiles y los guarda en una tabla en SQL.
        """
        pass

    @abc.abstractmethod
    def run(self):
        """
        Ejecuta el juego
        """
        pass
