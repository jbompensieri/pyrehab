"""utils.py 
Variables y funciones útiles para los archivos.
"""
import os
import pygame_menu

src_dir = os.path.dirname(__file__)  #: Path a la carpeta src
plugin_dirname = "Plugins"
plugin_dir = os.path.join(src_dir, plugin_dirname)  #: Path a la carpeta de plugins
data_dir = os.path.abspath(os.path.join(src_dir, "data"))  #: Path a la carpeta de data
#: Paths a carpetas que tienen imagenes y sonidos para los juegos
assets_dir = os.path.abspath(os.path.join(src_dir, "assets"))
memotest_dir = os.path.abspath(os.path.join(assets_dir, "memotest"))
memotest1_dir = os.path.abspath(os.path.join(memotest_dir, "argentina"))
memotest2_dir = os.path.abspath(os.path.join(memotest_dir, "monumentos"))
memotest3_dir = os.path.abspath(os.path.join(memotest_dir, "banderas"))
memotest4_dir = os.path.abspath(os.path.join(memotest_dir, "monedas"))
games_sound = os.path.abspath(os.path.join(assets_dir, "sonidos"))
escalas_dir = os.path.abspath(os.path.join(assets_dir, "escalas"))
diferencias_dir = os.path.abspath(os.path.join(assets_dir, "diferencias"))
diferencias1_dir = os.path.abspath(os.path.join(diferencias_dir, "facil"))
diferencias2_dir = os.path.abspath(os.path.join(diferencias_dir, "medio"))
diferencias3_dir = os.path.abspath(os.path.join(diferencias_dir, "dificil"))

# Variables de colores
red = (100, 0, 0)
light_red = (255, 0, 0)
green = (0, 100, 0)
green_unsam = (0, 220, 140)
lightgreen_unsam = (215, 255, 210)
yellow = (100, 100, 0)
light_yellow = (255, 255, 0)
blue = (22, 50, 92)
light_blue = (130, 160, 195)
white = (255, 255, 255)
black = (0, 0, 0)
light_black = (240, 240, 240)
ancho_menu_view = 600
alto_menu_view = 400

# Defino tema para PyRehab del jugador
mytheme = pygame_menu.themes.THEME_BLUE.copy()
fuente = os.path.join(assets_dir, "SairaUNSAM-MediumSC.ttf")
mytheme.title_font = fuente
mytheme.title_font_color = white
mytheme.title_font_shadow = False
mytheme.title_background_color = green_unsam
mytheme.widget_font = fuente
mytheme.widget_font_color = green_unsam
mytheme.widget_background_color = lightgreen_unsam
mytheme.background_color = lightgreen_unsam


def blit_text(surface, text, pos, font, color):
    """
    Función para mostrar un texto adaptado al tamaño de la pantalla.
    : param surface: Pantalla
    : param text: Texto a imprimir
    : param pos: Posición (x,y) donde inicia el texto
    : param font: Fuente del texto
    : param color: Color del texto
    """
    words = [
        word.split(" ") for word in text.splitlines()
    ]  #: Array donde cada fila es una lista de palabras
    space = font.size(" ")[0]  #: Tamaño del espacio
    max_width, max_height = surface.get_size()  #: Tamaño de la pantalla
    x, y = pos
    for line in words:
        for word in line:
            word_surface = font.render(word, 0, color)
            word_width, word_height = word_surface.get_size()
            if x + word_width >= max_width:
                x = pos[0]  # Reset the x.
                y += word_height  # Start on new row.
            surface.blit(word_surface, (x, y))
            x += word_width + space
        x = pos[0]  # Reset the x.
        y += word_height  # Start on new row.


def get_plugin_names():
    """
    Obtiene los nombres de los Plugins en una lista.
    : return: Lista con los nombres de los plugins
    """
    return [p[:-3] for p in os.listdir(plugin_dir) if p[-3:] == ".py"]
