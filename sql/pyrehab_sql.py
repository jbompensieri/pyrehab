"""pyrehab_sql.py 
Funciones para la creación y uso de la base de datos.
Utiliza tablas en mySQL"""
from argparse import _MutuallyExclusiveGroup
import mysql.connector
from sqlalchemy import create_engine
import pandas as pd


class pyrehab_sql:
    """Clase pyrehab_sql que va a contener las funciones para gestionar la base de datos."""

    def __init__(self):
        """
        Inicialización
        """
        self.db = self.get_db()
        self.mycursor = self.db.cursor()  #: Creo objeto cursor

    def get_db(self):
        """
        Obtener la base de datos
        """
        # pyr_db = mysql.connector.connect(
        #     host="localhost", user="jose", password="jose", database="pyrehab_database"
        # )  #: Entabla conexión con la base de datos (localhost)
        pyr_db = mysql.connector.connect(
            host="sql10.freesqldatabase.com",
            user="sql10594474",
            password="Ae5P1wCq9g",
            database="sql10594474",
        )  #: Entabla conexión con la base de datos (server)
        return pyr_db

    def get_df(self, user, table_name):
        """
        Obtener DataFrame con los resultados de los juegos
        : param user: DNI del jugador
        : param table_name: nombre de la tabla a extraer (coincide con el nombre del juego)
        : return: DataFrame con todas las columnas de la tabla, para ese usuario
        """
        # db_connection_str = 'mysql+pymysql://jose:jose@localhost/pyrehab_database' #: Conexión para localhost
        db_connection_str = "mysql+pymysql://sql10594474:Ae5P1wCq9g@sql10.freesqldatabase.com/sql10594474"  #: Conexión para server
        db_connection = create_engine(db_connection_str)
        query = f"SELECT * FROM {table_name} WHERE dni = '{user}'"
        db = pd.read_sql(query, con=db_connection)  #: Función de pandas para leer sql
        df = pd.DataFrame(db)  #: Convierto la base de datos a un DataFrame
        return df

    def create_table(self, table_name, table_columns):
        """
        Función para crear tablas.
        : param table_name: Nombre de la tabla
        : param table_columns: Nombres de las columnas
        : return: True si la tabla fue creada correctamente. En caso de error, retorna False
        """
        try:
            self.mycursor.execute(
                f"CREATE TABLE {table_name} ({table_columns})"
            )  #: Comando para crear la tabla
        except mysql.connector.errors.ProgrammingError as e:
            return False
        self.db.commit()  #: Commit cambios
        return True

    def add_row(self, table_name, row_values, columns=None):
        """
        Función para agregar una fila a la tabla.
        : param table_name: Nombre de la tabla
        : param row_values: Valores de la fila. Sintaxis: (valor1, ..., valorN)
        : param columns: Nombre de las columnas donde insertar los valores
        : return: True si el comando se ejecutó correctamente. Si ocurrió un error, retorna False
        """
        if columns:
            #: Coloca los valores en las columnas solicitadas
            command = f"INSERT INTO {table_name} {columns} VALUES {row_values}"
        else:
            #: Coloca los valores en orden, sin indicar el nombre de las columnas
            command = f"INSERT INTO {table_name} VALUES {row_values}"
        try:
            self.mycursor.execute(command)
            ok_flag = True
        except mysql.connector.errors.IntegrityError or mysql.connector.errors.ProgrammingError:
            ok_flag = False
        self.db.commit()  #: Commit cambios
        return ok_flag

    def edit_row(self, command):
        """
        Función para editar la fila de una tabla.
        : param command: Comando de MySQL para generar la edición
        """
        self.mycursor.execute(
            command
        )  #: Ejecuta comando que modifica los valores de una fila
        self.db.commit()  #: Commit cambios

    def delete_row(self, table_name, condition):
        """
        Función para eliminar una fila de la tabla.
        : param table_name: Nombre de la tabla
        : param condition: Condición para encontrar la fila y seleccionarla para eliminar
        """
        self.mycursor.execute(
            f"DELETE FROM {table_name} WHERE {condition}"
        )  #: Comando para eliminar fila
        self.db.commit()  #: Commit cambios

    def select_from_table(self, table_name, columns_l="*", condition=""):
        """
        Función para seleccionar valores de las columnas-
        : param table_name: Nombre de la tabla
        : param columns_l: Lista de nombres de las columnas. Por default se seleccionan todas
        : param condition: Condición para extraer valores de una fila en particular. Por defaul la condición está vacía
        : return: Lista de tuplas con el resltado de ejecutar el comando
        """
        if not condition:
            #: Si no hay condición, extrae valores de todas las filas
            self.mycursor.execute(f"SELECT {columns_l} FROM {table_name}")
        else:
            #: Si existe condición, extrae valores de la fila en particular.
            self.mycursor.execute(
                f"SELECT {columns_l} FROM {table_name} WHERE {condition}"
            )
        return self.mycursor.fetchall()  #: Resultado de la ejecución del comando

    def update_table(self, table_name, column, row_values, condition):
        """
        Función para actualizar la tabla.
        : param table_name: Nombre de la tabla
        : param column: Nombre de la columna
        : param row_values: Valores de la fila
        : param condition: Condición para ejecutar el comando
        """
        command = f"UPDATE {table_name} SET {column} = '{row_values}' WHERE {condition}"  #: Comando
        self.mycursor.execute(command)  #: Ejecuta el comando
        self.db.commit()  #: Commit cambios
