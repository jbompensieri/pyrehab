[[_TOC_]]
# Bienvenido a PyRehab  

## Introducción  
Este proyecto fue realizado en el marco de la materia Proyecto Final Integrador de la carrera de 
Ingeniería Biomédica de la Universidad Nacional de San Martín (Buenos Aires, Argentina).
Tiene como objetivo desarrollar un prototipo de plataforma de estimulación cognitiva accesible desde PC.

## Estado del proyecto
Esta primera versión está pensada para adultos mayores sanos, para la prevención del deterioro cognitivo
propio por el paso del tiempo. 
Contiene 3 juegos para estimulación de la memoria, la percepción visual y la atención. 
Cada juego tiene disponible 3 niveles (fácil, medio y difícil).
Es accesible para PC.
Se espera que en un futuro este desarrollo pueda seguir creciendo en cantidad de juegos, población alcanzada y
plataformas accesibles.

## Programación
Fue programado en Python 3.9.6  
Se utilizaron las siguientes librerías principales:  
* pygame = 2.0.1  
* pygame-menu = 4.1.6  
* pyinstaller = 5.7.0  
* PyQt5 = 5.15.6  
* mysql-connector-python = 8.0.29  
(más información en versiones.txt)  

## FAQ
1. ¿Cómo ejecuto los juegos?  
Para ejecutar los juegos desde la terminal se debe correr el archivo pyrehab.py en la carpeta src.  
1. ¿Cómo ejecuto la interfaz gráfca del profesional?  
Para ejecutar la interfaz gráfica desde la terminal se debe 
correr el archivo menu_prof.py en la carpeta pyqt.
1. ¿Cómo accedo por primera vez?
Para acceder a la interfaz de usuario profesional por primera vez se debe hacer con el usuario admin y la contraseña 1234.  
Para ello, el programador deberá crear en la tabla prof_table este usuario y su contraseña, encriptada.
1. ¿Cómo agrego juegos?  
Para agregar juegos o plugins, deberá generar el archivo en la carpeta src/Plugins, respetando los métodos mencionados en el archivo PluginABC.py.  
Estos métodos deben estar presente en todos los plugins para asegurar el funcionamiento del programa.  
1. ¿Cómo se encriptan las contraseñas?  
Las contraseñas se encriptan utilizando la librería cryptography.fernet en el archivo pyrehab_crypt.py en la carpeta pyqt.  
En ese archivo se encuentran los métodos de encriptación, desencriptación y es el encargado de crear la "key" correspondiente.  
Previa a la instalación de los programas, se deben correr las últimas líneas de código del archivo pyrehab_crypt.py. De esa forma, se genera la "key" y se obtiene la contraseña del usuario administrador, que luego debe colocarse en la tabla prof_table para garantizar el primer acceso.   
1. ¿Cómo creo la base de datos?  
Para crear la base de datos se utilizó MySQLWorkbench 8.0.  
Desde este programa, se generaron las tablas correspondientes:  
* user_table: para almacenar la información de los jugadores  
* prof_table: para almacenar la información de los profesionales de salud
* memotest, diferencias, escalas: cada una de ellas corresponde a un plugin. Almacena la información de los resultados de cada juego.  


### Contacto  
Para más información, está disponible en el repositorio el manuscrito del proyecto.  
        